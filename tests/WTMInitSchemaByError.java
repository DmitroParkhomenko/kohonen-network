import coloring.ColoringTopologicalOrderdMap;
import learning_factor_functional.GaussFunctionalFactor;
import learning_factor_functional.LearningFactorFunctionalModel;
import learning_factor_functional.LinearFunctionalFactor;
import main.Settings;
import main.WTMLearningBuilderWithInitSchemaByError;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import document.core.File2VectorInterface;
import utils.File2Matrix;

import java.io.IOException;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * Created by dmitro on 16.12.2014.
 */

@RunWith(Parameterized.class)
public class WTMInitSchemaByError {
    private static Logger _logger = LoggerFactory.getLogger(WTMInitSchemaByError.class);
    private ArrayList<Double[]> vectors;
    private boolean isRandomWeight;
    private int expectedClustersNumber;
    private int rowNumber;
    private int colNumber;
    private int neighborhoodRadius;
    private LearningFactorFunctionalModel functionalModel;
    private Double threshold;
    private Double error;

    public WTMInitSchemaByError(
            ArrayList<Double[]> vectors,
            boolean isRandomWeight,
            int expectedClustersNumber,
            int rowNumber,
            int colNumber,
            int neighborhoodRadius,
            LearningFactorFunctionalModel functionalModel,
            Double threshold,
            Double error
    ) {
        this.vectors = vectors;
        this.isRandomWeight = isRandomWeight;
        this.expectedClustersNumber = expectedClustersNumber;
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
        this.neighborhoodRadius = neighborhoodRadius;
        this.functionalModel = functionalModel;
        this.threshold = threshold;
        this.error = error;
    }

    @Parameterized.Parameters
    public static Collection testData() {

//        GaussFunctionalFactor functionalFactor = new GaussFunctionalFactor();
//        Double[] parameters = new Double[1];
//        parameters[0] = 3.0;
//        functionalFactor.setParameters(parameters);

        LearningFactorFunctionalModel functionalFactor = new LinearFunctionalFactor(0.5, 50.0);

        int rowSize = 10;

        ArrayList<Double[]> convert = new File2Matrix().convert("normalized_kegg_metavolic_relation_network.txt", rowSize);
//        ArrayList<Double[]> convert = new File2Matrix().convert("normalized_3D_spatial_network.txt", rowSize);

        return Arrays.asList(new Object[][]{
//                {//0
//                        convert,
//                        false,
//                        4,
//                        rowSize,
//                        23,
//                        1,
//                        functionalFactor,
//                        1.0,
//                        1E-2
//                }
                {//0
                        convert,
                        false,
                        4,
                        rowSize,
                        23,
                        1,
                        functionalFactor,
                        0.01,
                        1E-10,
                }

//                {//0
//                        new File2Matrix().convert("normalized_3D_spatial_network.txt", 10000),
//                        false,
//                        4,
//                        10000,
//                        4,
//                        1,
//                        functionalFactor,
//                        1.0,
//                        0.00001
//                }
//                {//1
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        1,
//                        5,
//                        5,
//                        1,
//                        0.8,
//                        1.0
//                },
//                {//2
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        2,
//                        5,
//                        5,
//                        1,
//                        0.8,
//                        1.0
//                },
//                {//3
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        5,
//                        5,
//                        1,
//                        0.8,
//                        1.0
//                },
//                {//4
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,//isRandomWeight
//                        5,//expectedClustersNumber
//                        5,//rowNumber
//                        5,//colNumber
//                        1,//neighborhoodRadius
//                        0.8,//constantFunctionalFactor
//                        1.0 //threshold for new cluster creation
//                },
        });
    }

    @Test
    public void test_case_by_error() throws IOException {

        Settings settings = new Settings();
        settings.setRandomWeight(isRandomWeight);
        settings.setRowNumber(rowNumber);
        settings.setColNumber(colNumber);
        settings.setRadius(neighborhoodRadius);
        settings.setFunctionalFactor(functionalModel);
        settings.setThreshold(threshold);
        settings.setErrorLimit(error);

        ColoringTopologicalOrderdMap result = byError(vectors, settings);

        Set<Integer> set = new HashSet<Integer>();

        for(Double[] item: vectors) {
            set.add(result.recognizePatternForCluster(item));
        }

        assertEquals(expectedClustersNumber, set.size());
    }

    private ColoringTopologicalOrderdMap byError(ArrayList<Double[]> list, Settings settings) throws IOException {
        File2VectorInterface mock = EasyMock.createNiceMock(File2VectorInterface.class);
        EasyMock.expect(mock.getDataList()).andReturn(list);
        EasyMock.expect(mock.getDataSize()).andReturn(list.size()).times(10000);

        for(int index = 0; index < list.size(); index++) {
            EasyMock.expect(mock.getData(index)).andReturn(list.get(index)).times(10000);
        }

        EasyMock.replay(mock);

        long timer = -System.currentTimeMillis();

        WTMLearningBuilderWithInitSchemaByError testObj = new WTMLearningBuilderWithInitSchemaByError();
        testObj.buildNetwork(mock, settings);

        timer += System.currentTimeMillis();
        _logger.info("test WTM Learning With Schema By Error: time = " + timer);

        testObj.saveAndColoringNetwork();
//        EasyMock.verify(mock);//for checking invocation count
        return testObj.getColoringTopologicalOrderdMap();
    }
}
