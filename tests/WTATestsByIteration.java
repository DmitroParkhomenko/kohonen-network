import coloring.ColoringTopologicalOrderdMap;
import learning_factor_functional.GaussFunctionalFactor;
import learning_factor_functional.LearningFactorFunctionalModel;
import main.Settings;
import main.WTALearningBuilderByIteration;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import document.core.File2VectorInterface;
import utils.File2Matrix;

import java.io.IOException;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * Created by dmitro on 19.12.2014.
 */

@RunWith(Parameterized.class)
public class WTATestsByIteration {

    private static Logger _logger = LoggerFactory.getLogger(WTATestsByIteration.class);

    private ArrayList<Double[]> vectors;
    private boolean isRandomWeight;
    private int expectedClustersNumber;
    private int maxIteration;
    private LearningFactorFunctionalModel functionalModel;
    private int rowNumber;
    private int colNumber;

    public WTATestsByIteration(
            ArrayList<Double[]> vectors,
            boolean isRandomWeight,
            int expectedClustersNumber,
            int rowNumber,
            int colNumber,
            LearningFactorFunctionalModel functionalModel,
            int maxIteration
    ) {
        this.vectors = vectors;
        this.isRandomWeight = isRandomWeight;
        this.expectedClustersNumber = expectedClustersNumber;
        this.maxIteration = maxIteration;
        this.functionalModel = functionalModel;
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
    }

    @Parameterized.Parameters
    public static Collection testData() {

        GaussFunctionalFactor functionalFactor = new GaussFunctionalFactor();
        Double[] parameters = new Double[1];
        parameters[0] = 0.5;
        functionalFactor.setParameters(parameters);

        return Arrays.asList(new Object[][]{
                {
                        new File2Matrix().convert("normalized_3D_spatial_network.txt", 10000),
                        false,
                        4,
                        10000,
                        4,
                        functionalFactor,
                        1
                }
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        1,
//                        30,
//                        0.8
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        2,
//                        30,
//                        0.8
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        30,
//                        0.8
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        30,
//                        0.8
//                },
        });
    }


    @Test
    public void test_case_by_iteration() throws IOException {

        Settings settings = new Settings();
        settings.setRandomWeight(isRandomWeight);
        settings.setMaxIteration(maxIteration);
        settings.setRowNumber(rowNumber);
        settings.setColNumber(colNumber);
        settings.setFunctionalFactor(functionalModel);

        ColoringTopologicalOrderdMap result =  byIteration(vectors, settings);

        Set<Integer> set = new HashSet<Integer>();

        for(Double[] item: vectors) {
            set.add(result.recognizePatternForCluster(item));
        }

        assertEquals(expectedClustersNumber, set.size());
    }

    private ColoringTopologicalOrderdMap byIteration(ArrayList<Double[]> list, Settings settings) throws IOException {
        File2VectorInterface mock = EasyMock.createNiceMock(File2VectorInterface.class);
        EasyMock.expect(mock.getDataList()).andReturn(list);
        EasyMock.expect(mock.getDataSize()).andReturn(list.size()).times(10000);

        for(int index = 0; index < list.size(); index++) {
            EasyMock.expect(mock.getData(index)).andReturn(list.get(index)).times(10000);
        }

        EasyMock.replay(mock);

        long timer = -System.currentTimeMillis();

        WTALearningBuilderByIteration testObj = new WTALearningBuilderByIteration();
        testObj.buildNetwork(mock, settings);
        timer += System.currentTimeMillis();

        testObj.saveAndColoringNetwork();

//        EasyMock.verify(mock);//for checking invocation count
        _logger.info("test WTA Learning By Iteration: time = " + timer);

        return testObj.getColoringTopologicalOrderdMap();
    }
}
