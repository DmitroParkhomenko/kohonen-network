import coloring.ColoringTopologicalOrderdMap;
import learning_factor_functional.GaussFunctionalFactor;
import learning_factor_functional.LearningFactorFunctionalModel;
import learning_factor_functional.LinearFunctionalFactor;
import main.*;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import document.core.File2VectorInterface;
import utils.File2Matrix;

import java.io.IOException;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * Created by dmitro on 11.11.2014.
 */

@RunWith(Parameterized.class)
public class WTATestsByError {
    private static Logger _logger = LoggerFactory.getLogger(WTATestsByError.class);
    private ArrayList<Double[]> vectors;
    private boolean isRandomWeight;
    private int expectedClustersNumber;
    private LearningFactorFunctionalModel functionalModel;
    private Double error;
    private int rowNumber;
    private int colNumber;

    public WTATestsByError(
            ArrayList<Double[]> vectors,
            boolean isRandomWeight,
            int expectedClustersNumber,
            int rowNumber,
            int colNumber,
            LearningFactorFunctionalModel functionalModel,
            Double error
    ) {
        this.vectors = vectors;
        this.isRandomWeight = isRandomWeight;
        this.expectedClustersNumber = expectedClustersNumber;
        this.functionalModel = functionalModel;
        this.error = error;
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
    }

    @Parameterized.Parameters
    public static Collection testData() {

//        GaussFunctionalFactor functionalFactor = new GaussFunctionalFactor();
//        Double[] parameters = new Double[1];
//        parameters[0] = 0.5;
//        functionalFactor.setParameters(parameters);
        LearningFactorFunctionalModel functionalFactor = new LinearFunctionalFactor(0.5, 10000.0);

        int rowSize = 2;

        return Arrays.asList(new Object[][]{
                {
                        new File2Matrix().convert("normalized_kegg_metavolic_relation_network.txt", rowSize),
                        false,
                        4,
                        rowSize,
                        23,
                        functionalFactor,
                        1E-5
                }
//                {
//                        new File2Matrix().convert("normalized_3D_spatial_network.txt", 1000),
//                        false,
//                        4,
//                        1000,
//                        4,
//                        functionalFactor,
//                        0.00001
//                }





//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 0, 0, 0});
//                            add(new Double[]{1, 1, 0, 0, 0});
//                            add(new Double[]{0, 0, 1, 0, 0});
//                            add(new Double[]{0, 0, 0, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        4,
//                        0.8
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        1,
//                        0.8
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        2,
//                        0.8
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        0.8
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        0.8
//                }
        });
    }

    @Test
    public void test_case_by_error() throws IOException {

        Settings settings = new Settings();
        settings.setRandomWeight(isRandomWeight);
        settings.setRowNumber(rowNumber);
        settings.setColNumber(colNumber);
        settings.setFunctionalFactor(this.functionalModel);
        settings.setErrorLimit(error);

        ColoringTopologicalOrderdMap result = byError(vectors, settings);

        Set<Integer> set = new HashSet<Integer>();

        for(Double[] item: vectors) {
            set.add(result.recognizePatternForCluster(item));
        }

        assertEquals(expectedClustersNumber, set.size());
    }

    private ColoringTopologicalOrderdMap byError(ArrayList<Double[]> list, Settings settings) throws IOException {

        File2VectorInterface mock = EasyMock.createNiceMock(File2VectorInterface.class);
        EasyMock.expect(mock.getDataList()).andReturn(list);
        int count = 1000000;
        EasyMock.expect(mock.getDataSize()).andReturn(list.size()).times(count);

        for(int index = 0; index < list.size(); index++) {
            EasyMock.expect(mock.getData(index)).andReturn(list.get(index)).times(count);
        }

        EasyMock.replay(mock);

        long timer = -System.currentTimeMillis();

        WTALearningBuilderByError testObj = new WTALearningBuilderByError();
        testObj.buildNetwork(mock, settings);

        timer += System.currentTimeMillis();

        testObj.saveAndColoringNetwork();

//        EasyMock.verify(mock);//for checking invocation count
        _logger.info("testWTALearningBuilderByError: time = " + timer);
        return testObj.getColoringTopologicalOrderdMap();
    }
}