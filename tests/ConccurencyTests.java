import document.concurrency.dictionary.DictionaryRunner;
import document.concurrency.file_processing.File2VectorRunner;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by dmitro on 09.03.2015.
 */
public class ConccurencyTests {

//    @Test
    public void create_dictionary_test() {
        DictionaryRunner runner = new DictionaryRunner();
        runner.run();
    }

    @Test
    public void create_trainy_vectors_from_dict() {
        File2VectorRunner runner = new File2VectorRunner();
        runner.run();

        List<Double[]> list = runner.getDataList();

        for (Iterator<Double[]> iterator = list.iterator(); iterator.hasNext(); ) {
            Double[] next = iterator.next();
            System.out.println(Arrays.toString(next));
        }
    }
}
