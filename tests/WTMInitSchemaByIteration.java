import coloring.ColoringTopologicalOrderdMap;
import learning_factor_functional.LearningFactorFunctionalModel;
import main.Settings;
import main.WTMLearningBuilderWithInitSchemaByIteration;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import document.core.File2VectorInterface;

import java.io.IOException;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * Created by dmitro on 20.12.2014.
 */

@RunWith(Parameterized.class)
public class WTMInitSchemaByIteration {
    private static Logger _logger = LoggerFactory.getLogger(WTMInitSchemaByIteration.class);
    private ArrayList<Double[]> vectors;
    private boolean isRandomWeight;
    private int expectedClustersNumber;
    private int rowNumber;
    private int colNumber;
    private int neighborhoodRadius;
    private int maxIteration;
    private LearningFactorFunctionalModel functionalModel;
    private Double threshold;

    public WTMInitSchemaByIteration(
            ArrayList<Double[]> vectors,
            boolean isRandomWeight,
            int expectedClustersNumber,
            int maxIteration,
            int rowNumber,
            int colNumber,
            int neighborhoodRadius,
            LearningFactorFunctionalModel functionalModel,
            Double threshold
    ) {
        this.vectors = vectors;
        this.isRandomWeight = isRandomWeight;
        this.expectedClustersNumber = expectedClustersNumber;
        this.maxIteration = maxIteration;
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
        this.neighborhoodRadius = neighborhoodRadius;
        this.functionalModel = functionalModel;
        this.threshold = threshold;
    }

    @Parameterized.Parameters
    public static Collection testData() {
        return Arrays.asList(new Object[][]{
                {
                        new ArrayList<Double[]>() {{
                            add(new Double[]{1.0, 1.0, 0.0, 0.0, 0.0});
                            add(new Double[]{1.0, 1.0, 0.0, 0.0, 0.0});
                            add(new Double[]{0.0, 0.0, 1.0, 0.0, 0.0});
                            add(new Double[]{0.0, 0.0, 0.0, 1.0, 1.0});
                            add(new Double[]{0.0, 0.0, 0.0, 0.0, 0.0});
                        }},
                        false,
                        4,
                        30,
                        5,
                        5,
                        1,
                        0.8,
                        1.0
                }
//                ,
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        1,
//                        30,
//                        5,
//                        5,
//                        1,
//                        0.8,
//                        1.0
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        2,
//                        30,
//                        5,
//                        5,
//                        1,
//                        0.8,
//                        1.0
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        30,
//                        5,
//                        5,
//                        1,
//                        0.8,
//                        1.0
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,//isRandomWeight
//                        5,//expectedClustersNumber
//                        30,//max iteration number
//                        5,//rowNumber
//                        5,//colNumber
//                        1,//neighborhoodRadius
//                        0.8,//constantFunctionalFactor
//                        1.0//threshold for new cluster creation
//                }
        });
    }

    @Test
    public void test_case_by_iteration_5() throws IOException {

        Settings settings = new Settings();
        settings.setRandomWeight(isRandomWeight);
        settings.setMaxIteration(maxIteration);
        settings.setRowNumber(rowNumber);
        settings.setColNumber(colNumber);
        settings.setRadius(neighborhoodRadius);
        settings.setFunctionalFactor(functionalModel);
        settings.setThreshold(threshold);

        ColoringTopologicalOrderdMap result =  byIteration(vectors, settings);

        Set<Integer> set = new HashSet<Integer>();

        for(Double[] item: vectors) {
            set.add(result.recognizePatternForCluster(item));
        }

        assertEquals(expectedClustersNumber, set.size());
    }

    private ColoringTopologicalOrderdMap byIteration(ArrayList<Double[]> list, Settings settings) throws IOException {
        File2VectorInterface mock = EasyMock.createNiceMock(File2VectorInterface.class);
        EasyMock.expect(mock.getDataList()).andReturn(list);
        EasyMock.expect(mock.getDataSize()).andReturn(list.size()).times(10000);

        for(int index = 0; index < list.size(); index++) {
            EasyMock.expect(mock.getData(index)).andReturn(list.get(index)).times(10000);
        }

        EasyMock.replay(mock);

        long timer = -System.currentTimeMillis();

        WTMLearningBuilderWithInitSchemaByIteration testObj = new WTMLearningBuilderWithInitSchemaByIteration();
        testObj.buildNetwork(mock, settings);

        timer += System.currentTimeMillis();
        _logger.info("testWTALearningBuilderByError: time = " + timer);
        testObj.saveAndColoringNetwork();

//        EasyMock.verify(mock);//for checking invocation count

        return testObj.getColoringTopologicalOrderdMap();
    }
}
