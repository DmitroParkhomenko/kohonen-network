import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import topology.MatrixTopology;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * Created by dmitro on 17.12.2014.
 */

@RunWith(Parameterized.class)
public class NeighbourhoodMatrixTests {
    private int row;
    private int col;
    private int radius;
    private int neuronEntryPoint;
    private String expectedResult;

    public NeighbourhoodMatrixTests(
            int row,
            int col,
            int radius,
            int neuronEntryPoint,
            String expectedResult
    ) {
        this.row = row;
        this.col = col;
        this.radius = radius;
        this.neuronEntryPoint = neuronEntryPoint;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static Collection testData() {
        return Arrays.asList(new Object[][]{
                {5, 5, 1, 0, "[0, 1, 5]"},
                {5, 5, 2, 0, "[0, 1, 2, 5, 6, 10]"},
                {5, 5, 1, 12, "[7, 11, 12, 13, 17]"},
                {5, 5, 2, 12, "[2, 6, 7, 8, 10, 11, 12, 13, 14, 16, 17, 18, 22]"},
                {5, 5, 1, 11, "[6, 10, 11, 12, 16]"},
                {5, 5, 1, 4, "[3, 4, 9]"},
                {5, 5, 2, 4, "[2, 3, 4, 8, 9, 14]"},
                {5, 5, 1, 18, "[13, 17, 18, 19, 23]"},
                {5, 5, 2, 18, "[8, 12, 13, 14, 16, 17, 18, 19, 22, 23, 24]"},
                {5, 5, 1, 20, "[15, 20, 21]"},
                {5, 5, 1, 24, "[19, 23, 24]"},
                {5, 5, 3, 12, "[1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23]"},
                {5, 5, 4, 12, "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]"},
        });
    }

    @Test
    public void neighbourhood() {
        MatrixTopology matrixTopology = new MatrixTopology( this.row, this.col, this.radius);
        TreeSet<Integer> actual = matrixTopology.neighbourhoodBuilder( this.neuronEntryPoint);
        assertEquals(this.expectedResult, actual.toString());
    }
}