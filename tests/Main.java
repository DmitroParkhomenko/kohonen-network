import coloring.ColoringTopologicalOrderdMap;
import document.concurrency.dictionary.DictionaryRunner;
import document.concurrency.file_processing.ClusterizationInterface;
import document.concurrency.file_processing.File2VectorRunner;
import document.core.File2VectorInterface;
import learning_factor_functional.GaussFunctionalFactor;
import main.Settings;
import main.WTMLearningBuilderWithInitSchemaByError;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

/**
 * Created by dmitro on 12.03.2015.
 */
public class Main {
    @Test
    public void test() throws IOException {
        //step 1
        final int MAX_WORDS_FROM_FILE_2_DICTIONARY = 100;
        DictionaryRunner dictionaryRunner = new DictionaryRunner("filesForDictionary", "dictionary.txt", MAX_WORDS_FROM_FILE_2_DICTIONARY);
//        DictionaryRunner dictionaryRunner = new DictionaryRunner("filesForTests", "dictionary.txt", 100);
        dictionaryRunner.run();

        //step 2
        File2VectorInterface file2VectorRunner = new File2VectorRunner("filesForDictionary", "dictionary.txt", Integer.MAX_VALUE);
        file2VectorRunner.run();

        //step 3
        Settings settings = new Settings();
        settings.setRandomWeight(false);
        settings.setRowNumber(5);
        settings.setColNumber(5);
        settings.setRadius(1);

        GaussFunctionalFactor functionalFactor = new GaussFunctionalFactor();
        Double[] parameters = new Double[1];
        parameters[0] = 0.5;
        functionalFactor.setParameters(parameters);
        settings.setFunctionalFactor(functionalFactor);
        settings.setThreshold(1.0);
        settings.setErrorLimit(0.00001);

        WTMLearningBuilderWithInitSchemaByError algorithm = new WTMLearningBuilderWithInitSchemaByError();
        algorithm.buildNetwork(file2VectorRunner, settings);

        //stem 4
        algorithm.saveAndColoringNetwork();
        ColoringTopologicalOrderdMap coloringTopologicalOrderdMap = algorithm.getColoringTopologicalOrderdMap();

        //step 5
        ClusterizationInterface clusterizationFiles = new File2VectorRunner("filesForClasterezation", "dictionary.txt", Integer.MAX_VALUE);
        clusterizationFiles.run();
        Map<String, Double[]> map = clusterizationFiles.geResults();

        for(String fileName: map.keySet()) {
            Double[] inputVector = map.get(fileName);
            int clusterId = coloringTopologicalOrderdMap.recognizePatternForCluster(inputVector);
            System.out.println("fileName = " + fileName +  ", cluster id = " + clusterId);
        }
    }
}
