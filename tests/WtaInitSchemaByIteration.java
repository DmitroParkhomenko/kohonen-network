import coloring.ColoringTopologicalOrderdMap;
import learning_factor_functional.LearningFactorFunctionalModel;
import main.Settings;
import main.WTALearningBuilderWithInitSchemaByIteration;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import document.core.File2VectorInterface;

import java.io.IOException;
import java.util.*;

import static junit.framework.Assert.assertEquals;

/**
 * Created by dmitro on 19.12.2014.
 */

@RunWith(Parameterized.class)
public class WtaInitSchemaByIteration {
    private static Logger _logger = LoggerFactory.getLogger(WtaInitSchemaByIteration.class);
    private ArrayList<Double[]> vectors;
    private boolean isRandomWeight;
    private int expectedClustersNumber;
    private int rowNumber;
    private int colNumber;
    private int maxIteration;
    private LearningFactorFunctionalModel functionalModel;
    private Double threshold;

    public WtaInitSchemaByIteration(
            ArrayList<Double[]> vectors,
            boolean isRandomWeight,
            int expectedClustersNumber,
            int rowNumber,
            int colNumber,
            int maxIteration,
            LearningFactorFunctionalModel functionalModel,
            Double threshold
    ) {
        this.vectors = vectors;
        this.isRandomWeight = isRandomWeight;
        this.expectedClustersNumber = expectedClustersNumber;
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
        this.maxIteration = maxIteration;
        this.functionalModel = functionalModel;
        this.threshold = threshold;
    }

    @Parameterized.Parameters
    public static Collection testData() {
        return Arrays.asList(new Object[][]{
                {
                        new ArrayList<Double[]>() {{
                            add(new Double[]{1.0, 1.0, 0.0, 0.0, 0.0});
                            add(new Double[]{1.0, 1.0, 0.0, 0.0, 0.0});
                            add(new Double[]{0.0, 0.0, 1.0, 0.0, 0.0});
                            add(new Double[]{0.0, 0.0, 0.0, 1.0, 1.0});
                            add(new Double[]{0.0, 0.0, 0.0, 0.0, 0.0});
                        }},
                        false,
                        4,
                        5,
                        5,
                        30,
                        0.8,
                        1.0
                }
//                ,
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        1,
//                        5,
//                        5,
//                        30,
//                        0.8,
//                        1.0
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{1, 1, 1, 1, 1});
//                        }},
//                        false,
//                        2,
//                        5,
//                        5,
//                        30,
//                        0.8,
//                        1.0
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        5,
//                        5,
//                        30,
//                        0.8,
//                        1.0
//                },
//                {
//                        new ArrayList<Double[]>() {{
//                            add(new Double[]{1, 1, 1, 1, 1});
//                            add(new Double[]{0, 1, 1, 1, 1});
//                            add(new Double[]{0, 0, 1, 1, 1});
//                            add(new Double[]{0, 0, 0, 1, 1});
//                            add(new Double[]{0, 0, 0, 0, 0});
//                        }},
//                        false,
//                        2,
//                        5,
//                        5,
//                        30,
//                        0.8,
//                        1.0
//                }
        });
    }

    @Test
    public void test_case_by_iteration() throws IOException {

        Settings settings = new Settings();
        settings.setRandomWeight(isRandomWeight);
        settings.setRowNumber(rowNumber);
        settings.setColNumber(colNumber);
        settings.setMaxIteration(maxIteration);
        settings.setFunctionalFactor(functionalModel);
        settings.setThreshold(threshold);

        ColoringTopologicalOrderdMap result =  byIteration(vectors, settings);

        Set<Integer> set = new HashSet<Integer>();

        for(Double[] item: vectors) {
            set.add(result.recognizePatternForCluster(item));
        }

        assertEquals(expectedClustersNumber, set.size());
    }

    private ColoringTopologicalOrderdMap byIteration(ArrayList<Double[]> list, Settings settings) throws IOException {
        File2VectorInterface mock = EasyMock.createNiceMock(File2VectorInterface.class);
        EasyMock.expect(mock.getDataList()).andReturn(list);
        EasyMock.expect(mock.getDataSize()).andReturn(list.size()).times(10000);

        for(int index = 0; index < list.size(); index++) {
            EasyMock.expect(mock.getData(index)).andReturn(list.get(index)).times(10000);
        }

        EasyMock.replay(mock);

        long timer = -System.currentTimeMillis();

        WTALearningBuilderWithInitSchemaByIteration testObj = new WTALearningBuilderWithInitSchemaByIteration();
        testObj.buildNetwork(mock, settings);

        timer += System.currentTimeMillis();
        _logger.info("testWTALearningBuilderByError: time = " + timer);
        testObj.saveAndColoringNetwork();

//        EasyMock.verify(mock);//for checking invocation count

        return testObj.getColoringTopologicalOrderdMap();
    }
}
