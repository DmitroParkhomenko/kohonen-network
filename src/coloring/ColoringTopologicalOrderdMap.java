package coloring;

import kohonen.LearningFunctionInterface;
import network.NetworkModelInterface;
import neuron.NeuronModel;
import topology.TopologyModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by dmitro on 12.06.2014.
 */

public class ColoringTopologicalOrderdMap {

    private LearningFunctionInterface functionInterface;
    private static Logger _logger = LoggerFactory.getLogger(ColoringTopologicalOrderdMap.class);

    public ColoringTopologicalOrderdMap(LearningFunctionInterface functionInterface) {
        this.functionInterface = functionInterface;
    }

    public int recognizePatternForCluster(Double[] inputVector) {
        int result = 0;
        int size = functionInterface.getNetworkModelInterface().getNumbersOfNeurons();
        Double currDistance = Double.MAX_VALUE;

        for(int index = 0; index < size; index++ ) {

            NeuronModel neuron = functionInterface.getNetworkModelInterface().getNeuron(index);
            Double[] weights = neuron.getWeight();
            Double distance = functionInterface.getMetrics().getDistance(weights, inputVector);

            if(distance < currDistance) {
                currDistance = distance;
                result = neuron.getClusterId();
            }
        }

        return result;
    }

    public void coloringMapWithTrainingData() {

        _logger.info("coloringMapWithTrainingData() run");

        int size = functionInterface.getLearningData().getDataSize();

        for(int index = 0; index < size; index++) {
            int clusterId = recognizePattern(functionInterface.getLearningData().getData(index));
            functionInterface.getNetworkModelInterface().getNeuron(clusterId).setClusterId(index);
        }
    }

    public void coloringFullMap() {

        _logger.info("coloringFullMap() run");

        int size = functionInterface.getNetworkModelInterface().getNumbersOfNeurons();
        NetworkModelInterface networkModel = functionInterface.getNetworkModelInterface();

        for(int index = 0; index < size; index++) {
            int curr = networkModel.getNeuron(index).getClusterId();

            if(curr == -1) {
                Double minDist = Double.MAX_VALUE;
                int currClusterId = 0;

                for(int innerIndex = 0; innerIndex < size; innerIndex++) {
                    int curr2 = networkModel.getNeuron(innerIndex).getClusterId();

                    if(curr2 != -1 && index != innerIndex) {
                        Double currDist = functionInterface.getMetrics().getDistance(
                                networkModel.getNeuron(index).getWeight(),
                                networkModel.getNeuron(innerIndex).getWeight());

                        if(currDist < minDist) {
                            minDist = currDist;
                            currClusterId = curr2;
                        }
                    }
                }

                networkModel.getNeuron(index).setClusterId(currClusterId);
            }
        }
    }

    public void printTopologyToConsole() {

        _logger.info("printTopologyToConsole() run");

        TopologyModel topologyModel = functionInterface.getNetworkModelInterface().getTopology();
        NetworkModelInterface networkModel = functionInterface.getNetworkModelInterface();

        for(int row = 0; row < topologyModel.getRowNumber(); row++) {
            for(int col = 0; col < topologyModel.getColNumber(); col++) {
                _logger.info(networkModel.getNeuron(row * topologyModel.getColNumber() + col).getClusterId() + " ");
            }

            _logger.info("---------------------------------------------------------------------------------------");
        }
    }

    private int recognizePattern(Double[] inputVector) {
        int result = 0;
        int size = functionInterface.getNetworkModelInterface().getNumbersOfNeurons();
        Double currDistance = Double.MAX_VALUE;

        for(int index = 0; index < size; index++ ) {

            NeuronModel neuron = functionInterface.getNetworkModelInterface().getNeuron(index);
            Double[] weights = neuron.getWeight();
            Double distance = functionInterface.getMetrics().getDistance(weights, inputVector);

            if(distance < currDistance) {
                currDistance = distance;
                result = index;
            }
        }

        return result;
    }
}