package metrics;

/**
 * Computes distance between pairs of objects.
 */
/**
 * Created by dmitro on 26.05.2014.
 */
public interface MetricModel {
    
    /**
     * Return value containing the distance information
     * @param firstVector first input vector
     * @param secondVector second input vector
     * @return distance inrormation
     */
    public Double getDistance(Double[] firstVector, Double[] secondVector);
}
