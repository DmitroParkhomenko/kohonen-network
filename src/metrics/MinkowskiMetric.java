package metrics;

/**
 * Minkowski metric return distance calculated by function:
 * {sum[(x_i - y_i)^p]}^(1/p)
 */

public class MinkowskiMetric implements MetricModel{
    
    /**
     * 
     *     Matric parameter. Default value is 1
     *     
     */
    private Double p = 1.0;
    
    /**
     * Creates a new instance of MinkowskiMetrics.
     * Default value of the parameter <I>p</I> is <I>1</I>
     * @param p metrics parameter
     */
    public MinkowskiMetric(Double p) {
        this.p = p;
    }
    
    /**
     * Set parameter <I>p</I>
     * @param paramateresList array of parameter
     */
    public void setParameteres(Double[] paramateresList) {
        p = paramateresList[0];
    }

    /**
     * Return array of parameters containing <I>p</I>
     * @return array of parameter containing p
     */
    public Double[] getParamateres() {
        Double [] parameter = new Double[1];
        parameter[0] = p;
        return parameter;
    }
    
    /**
     * Return distance beetwen input vectors. 
     * firstVector vector and secondVector must have the same size otherwise 
     * function return -1
     * d = {sum[(x_i - y_i)^p]}^(1/p)
     * @param firstVector first input vector
     * @param secondVector second input vector
     * @return distance beetwen vectors
     */
    public Double getDistance(Double[] firstVector, Double[] secondVector) {
        Double distance = 0.0;
        Double x = 0.0, w = 0.0;
        Double sum = 0.0;
        int weightLenght = firstVector.length;
        
        if(firstVector.length != secondVector.length)
            return -1.0;
        
        for(int i=0; i< weightLenght; i++){
            w = firstVector[i]; 
            x = secondVector[i];
            sum +=Math.pow(Math.abs(x - w),p);
        }
        distance = Math.pow(sum,1/p);
        return distance;
    }
}


