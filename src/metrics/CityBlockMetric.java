package metrics;

/**
 * City block metric return distance calculated by function:
 * sum|x_i - y_i| for each element from inputs vectors, where x_i is first input vector element,
 * y_i is second vector element.
 */

public class CityBlockMetric implements MetricModel{
    
    /** Creates a new instance of CityBlockMetric */
    public CityBlockMetric() {
    }
    
    /**
     * Return value containing the distance information. 
     * firstVector vector and secondVector must have the same size otherwise 
     * function return -1
     * @param firstVector first input vector
     * @param secondVector second input vector
     * @return distance information
     */
    public Double getDistance(Double[] firstVector, Double[] secondVector){
        Double distance = 0.0;
        Double x = 0.0, w = 0.0;
        int weightLenght = firstVector.length;
        
        if(firstVector.length != secondVector.length)
            return -1.0;
        
        for(int i=0; i< weightLenght; i++){
            w = firstVector[i]; 
            x = secondVector[i];
            distance += Math.abs(x - w);
        }
        return distance;
    }
}
