package metrics;

/**
 * Euclides metric return distance calculated by function:
 * sum[sqrt(x_i - y_i)] for each element from inputs vectors, where x_i is first input vector element,
 * y_i is second vector element.
 */

public class EuclidesMetric implements MetricModel{
    
    /** Creates a new instance of CityBlockMetric */
    public EuclidesMetric(){
    }
    
    /**
     * Return value containing the distance information. 
     * firstVector vector and secondVector must have the same size otherwise 
     * function return -1
     * @param firstVector first vector
     * @param secondVector second vector
     * @return distance information
     */
    public Double getDistance(Double[] firstVector, Double[] secondVector) {
        Double distance = 0.0;
        Double x = 0.0, w = 0.0;
        Double sum = 0.0;
        int weightSize = firstVector.length;
        
        if(weightSize != secondVector.length)
            return -1.0;
        
        for(int i=0; i< weightSize; i++) {

            w = firstVector[i]; 
            x = secondVector[i];
            sum += (x - w) *( x - w);
        }
        
        distance = Math.sqrt(sum);
        return distance;
    }
}
