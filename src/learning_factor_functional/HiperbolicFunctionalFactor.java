package learning_factor_functional;

/**
 * Hiperbolic Function describe by funtion <I>c1/(c2 + k) </I> <BR>
 * where: <BR>
 * <I>c1, c2 </I> - parameters
 * <I>k </I> - iteration number
 */
public class HiperbolicFunctionalFactor implements LearningFactorFunctionalModel {
    private Double c1;
    
    private Double c2;
    
    /**
     * Creates a new instance of HiperbolicFunctionalFactor with specified parameters <I>c1, c2</I>
     * @param c1 parameter
     * @param c2 parameter
     */
    public HiperbolicFunctionalFactor(Double c1, Double c2) {
        this.c1 = c1;
        this.c2 = c2;
    }
    
    /**
     * Get function parameters. First value at the array is <I>n0</I> second 
     * <I>maxIteration</I>
     * @return Array of parameters
     */
    public Double[] getParameters(){
        Double[] parameters = new Double[2];
        parameters[0] = c1;
        parameters[1] = c2;
        return parameters;
    }
    
    /**
     * Set funciton parameters. First value at the array is <I>n0</I> second 
     * <I>maxIteration</I>
     * @param parameters array of parameters
     */    
    public void setParameters(Double[] parameters){
        c1 = parameters[0];
        c2 = parameters[1];
    }
    
    /**
     * Return function value for specified iteratnion. Value is
     * independent of the iteration and is the same as parameters
     * @param k iternetion number
     * @return value of function factor
     */
    public Double getValue(int k){
        return c1/(c2 + k);
    }
}
