package learning_factor_functional;

/**
 * Gauss Function Factor descibed by function <I>exp(-(k^2))/(2*r^2))</I>
 */

public class GaussFunctionalFactor implements LearningFactorFunctionalModel{
    
    private Double r;   //radius
    
    /**
     * Return array containing parameter <I>r</I> - radius
     * @return constant parameter
     */
    public Double[] getParameters(){
        Double[] paremateres = new Double[1];
        paremateres[0] = r;
        return paremateres;    
    }
    
    /**
     * Set parameters. <I>r</I> - radius
     * @param parameters constat value
     */
    public void setParameters(Double[] parameters){
        r = parameters[0];
    }
    
    /**
     * Return value calculated by function describe as 
     * <I>exp(-(k^2))/(2*r^2))</I>
     * @param k iteration
     * @return value of function factor
     */
    public Double getValue(int k){
       return java.lang.Math.exp(-(java.lang.Math.pow(k,2))/ (2 * r * r ));
    }
}
