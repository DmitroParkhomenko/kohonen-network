package learning_factor_functional;

/**
 * Linear function describe by: <I> y = (n0 / maxIter) * (maxIter - iter) </I>  where: <br>
 * <I>y</I> - output value <br>
 * <I>n0</I> - maximal factor <br>
 * <I>maxIter</I> - maximal number of iteration <br>
 * <I>iter</I> - iteration number
 */

public class LinearFunctionalFactor implements LearningFactorFunctionalModel {
    private Double n0;
    
    private Double maxIteration;
    /**
     * Creates a new instance of LinearFunctionalFactor
     * @param n0 maximal factor
     * @param maxIteration maximal number of iteration
     */
    
    public LinearFunctionalFactor(Double n0, Double maxIteration) {
        this.n0 = n0;
        this.maxIteration = maxIteration;
    }
    
    /**
     * Get function parameters. First value at the array is <I>n0</I> second 
     * <I>maxIteration</I>
     * @return Array of parameters
     */
     public Double[] getParameters(){
         Double[] parameters= new Double[2];
         parameters[0] = n0;
         parameters[1] = maxIteration;
        return parameters;    
    }
    
    /**
     * Set funciton parameters. First value at the array is <I>n0</I> second 
     * <I>maxIteration</I>
     * @param parameters array of parameters
     */
    public void setParameters(Double[] parameters){
        n0 = parameters[0];
        maxIteration = parameters[1];
    }
    
    /**
     * Return funciton value for specified iteration
     * @param k iteration
     * @return funciton value for specified iteration
     */
    public Double getValue(int k){
        return (n0/maxIteration)*(maxIteration-k); 
    }
}

