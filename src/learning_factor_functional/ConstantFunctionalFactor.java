package learning_factor_functional;

/**
 * Constant Function factor. Return constant value indpenend
 * of the iteration number
 */
public class ConstantFunctionalFactor implements LearningFactorFunctionalModel{
    
    /**
     * constatn value
     */
    private Double constant;
    
    /**
     * Creates a new instance of ConstatnFunctionalModel
     * @param constant constant value
     */
    public ConstantFunctionalFactor(Double constant) {
        this.constant = constant;
    }
    
    /**
     * Return array containing parameter
     * @return constant parameter
     */
    public Double[] getParameters(){
        Double[] parameteres = new Double[1];
        parameteres[0] = constant;
        return parameteres;
    }
    
    /**
     * Set parameter
     * @param parameters constat value
     */
    public void setParameters(Double[] parameters){
        constant = parameters[0];
    }
    
    /**
     * Return function value for specified iteratnion. Value is
     * independent of the iteration and is the same as parameters
     * @param k iternetion number
     * @return value of function factor
     */
    public Double getValue(int k){
        return constant;
    }
}
