package learning_factor_functional;

/**
 * Expotentional Function Factor. Return value calculated by function:
 * indpenend <I>n0 * exp(-c*k)</I>  <BR>where:<BR>
 * <I>n0, c </I> - constatn <BR>
 * <I>k</I> - iteration number <BR>
 */
public class ExponentionalFunctionFactor implements LearningFactorFunctionalModel{
   
    private Double n0;
    
    
    private Double c;
    
    
    /**
     * Creates a new instance of ExponentionalFunctionFactor
     * @param n0 constant parameter
     * @param c constant parameter
     */
    public ExponentionalFunctionFactor(Double n0, Double c) {
        this.n0 = n0;
        this.c = c;
    }
    
     /**
     * Return array containing parameters. First parameter is <I>n0</I>, second
     * <I>c</I>
     * @return parameters array
     */
    
    public Double[] getParameters(){
        Double[] parameters = new Double[2];
        parameters[0] = n0;
        parameters[1] = c;
        return parameters;
    }
    
    /**
     * Set parameters
     * @param parameters parameters array
     */
    public void setParameters(Double[] parameters){
        n0 = parameters[0];
        c = parameters[1];
    }
    
    /**
     * Return value calculated by function describe by <I>n0 * exp(-c*k)</I>
     * @param k iteratnion number
     * @return value of function factor
     */
    public Double getValue(int k){
        return n0*java.lang.Math.exp(-c*k);
    }
}
