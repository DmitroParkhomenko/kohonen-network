package learning_factor_functional;

/**
 * Interface representing learning function model
 */

public interface LearningFactorFunctionalModel {
    
    /**
     * Get function parameters
     * @return Array of parameters
     */
    public Double[] getParameters();
    
    /**
     * Set funciton parameters
     * @param parameters array of parameters
     */
    public void setParameters(Double[] parameters);
    
    /**
     * Return funciton value for specified iteration
     * @param k iteration
     * @return funciton value for specified iteration
     */
    public Double getValue(int k);
    
}
