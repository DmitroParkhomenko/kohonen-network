package main;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by dmitro on 14.05.2014.
 */
public class Filter {
    public File[] finder( String dirName){
        File dir = new File(dirName);

        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename)
            { return filename.endsWith(".txt"); }
        } );
    }
}
