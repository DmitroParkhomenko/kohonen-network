package main;

/**
 * Created by dmitro on 12.05.2014.
 */
public final class Consts {
    public static final String FILE_ENC = "UTF-8";
    public static final String LEAN_FOLDER_NAME = "leanFiles";
    public static final String DICTIONARY_PATH = "dictionary.txt";
    public static final String[] SPEECH_PART = {"=A", "=S", "=V"};
    public static final String MYSTEM_PROGRAM = "mystem.exe";
    public static final String PARAMS = "-li";
    public static final String ENC_PATAM = "-e utf-8";
    public static final String DELIMETR_FOR_MYSTEM_TOKENS = "}";
}
