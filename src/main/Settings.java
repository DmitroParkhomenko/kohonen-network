package main;

import learning_factor_functional.LearningFactorFunctionalModel;

/**
 * Created by dmitro on 05.12.2014.
 */
public class Settings {
    private Double[] initVector;
    private boolean isRandomWeight;
    private int maxIteration;
    private int rowNumber;
    private int colNumber;
    private Double threshold;
    private int radius;
    private Double errorLimit;

    public Double[] getInitVector() {
        return initVector;
    }

    public void setInitVector(Double[] initVector) {
        this.initVector = initVector;
    }

    private LearningFactorFunctionalModel functionalFactor;

    public Double getErrorLimit() {
        return errorLimit;
    }

    public void setErrorLimit(Double errorLimit) {
        this.errorLimit = errorLimit;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public boolean isRandomWeight() {
        return isRandomWeight;
    }

    public void setRandomWeight(boolean isRandomWeight) {
        this.isRandomWeight = isRandomWeight;
    }

    public int getMaxIteration() {
        return maxIteration;
    }

    public void setMaxIteration(int maxIteration) {
        this.maxIteration = maxIteration;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public int getColNumber() {
        return colNumber;
    }

    public void setColNumber(int colNumber) {
        this.colNumber = colNumber;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public void setFunctionalFactor(LearningFactorFunctionalModel functionalFactor) {
        this.functionalFactor = functionalFactor;
    }

    public LearningFactorFunctionalModel getFunctionalFactor() {
        return functionalFactor;
    }
}
