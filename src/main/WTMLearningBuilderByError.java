package main;

import coloring.ColoringTopologicalOrderdMap;
import kohonen.LearningFunctionInterface;
import kohonen.WTMLearningFunction;
import metrics.EuclidesMetric;
import network.DefaultNetwork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import document.core.File2VectorInterface;
import topology.GaussNeighbourhoodFunction;
import topology.MatrixTopology;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by dmitro on 15.06.2014.
 */

public class WTMLearningBuilderByError {

    private static Logger _logger = LoggerFactory.getLogger(WTMLearningBuilderByError.class);
    private WTMLearningFunction learning;
    private ColoringTopologicalOrderdMap coloringTopologicalOrderdMap;

    public void buildNetwork(File2VectorInterface docsEncoderUtils, Settings settings) throws IOException {
        _logger.info("================================buildNetwork - started=======================================");

        MatrixTopology topology = new MatrixTopology(
                settings.getRowNumber(),
                settings.getColNumber(),
                settings.getRadius());

        Double[] maxWeight = new Double[docsEncoderUtils.getData(0).length];
        Arrays.fill(maxWeight, 1.0);

        DefaultNetwork network = new DefaultNetwork(docsEncoderUtils.getData(0).length, maxWeight, topology, settings);

        learning = new WTMLearningFunction(
                network,
                new EuclidesMetric(),
                docsEncoderUtils,
                settings.getFunctionalFactor(),
                new GaussNeighbourhoodFunction(settings.getRadius()));

        learning.setError(settings.getErrorLimit());
        learning.learnByMapError();

        _logger.info("=========================================buildNetwork - ended================================");
    }

    public void saveAndColoringNetwork() throws IOException {
        _logger.info("=========================================saveAndColoringNetwork START===========================================");

//        learning.getNetworkModelInterface().networkToFile("networkToFile.txt");
        doColoring(learning);

        _logger.info("=========================================saveAndColoringNetwork END===========================================");
    }

    public ColoringTopologicalOrderdMap getColoringTopologicalOrderdMap() {
        return coloringTopologicalOrderdMap;
    }

    private void doColoring(LearningFunctionInterface learning) {
        coloringTopologicalOrderdMap = new ColoringTopologicalOrderdMap(learning);
        coloringTopologicalOrderdMap.coloringMapWithTrainingData();
        coloringTopologicalOrderdMap.coloringFullMap();
//        coloringTopologicalOrderdMap.printTopologyToConsole();
    }
}
