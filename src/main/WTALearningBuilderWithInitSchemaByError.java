package main;

import coloring.ColoringTopologicalOrderdMap;
import efficient_initialization_scheme.InitializationScheme;
import kohonen.LearningFunctionInterface;
import kohonen.WTALearningFunction;
import learning_factor_functional.GaussFunctionalFactor;
import lvq.Cluster;
import lvq.LearningVectorQuantization;
import metrics.EuclidesMetric;
import network.DefaultNetwork;
import neuron.NeuronModel;
import document.core.File2VectorInterface;
import topology.MatrixTopology;

import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created by dmitro on 15.06.2014.
 */
public class WTALearningBuilderWithInitSchemaByError {

    private static Logger _logger = LoggerFactory.getLogger(WTALearningBuilderWithInitSchemaByError.class);
    private WTALearningFunction learning;
    private  ColoringTopologicalOrderdMap coloringTopologicalOrderdMap;

    public void buildNetwork(File2VectorInterface docsEncoderUtils, Settings settings) throws IOException {
        _logger.info("================================buildNetwork - started=======================================");

        LearningVectorQuantization lvq = new LearningVectorQuantization(settings.getThreshold());
        ArrayList<Cluster> clusters = lvq.run(docsEncoderUtils);

        MatrixTopology topology = new MatrixTopology(
                settings.getRowNumber(),
                settings.getColNumber());

        InitializationScheme scheme = new InitializationScheme(new EuclidesMetric(), topology);

        NeuronModel[] neuronModels = scheme.run(clusters);

        GaussFunctionalFactor gaussFunctionalFactor = new GaussFunctionalFactor();
        Double[] parameters = new Double[1];
        parameters[0] = 0.5;
        gaussFunctionalFactor.setParameters(parameters);

        learning = new WTALearningFunction(
                new DefaultNetwork(neuronModels, topology),
                new EuclidesMetric(),
                docsEncoderUtils,
                gaussFunctionalFactor
//                new ConstantFunctionalFactor(settings.getConstantFunctionalFactor())
        );

        learning.setError(settings.getErrorLimit());
        learning.learnByMapError();

        _logger.info("=========================================buildNetwork - ended================================");
    }

    public void saveAndColoringNetwork() throws IOException {
        _logger.info("=========================================saveAndColoringNetwork START===========================================");

        learning.getNetworkModelInterface().networkToFile("networkToFile.txt");
        doColoring(learning);

        _logger.info("=========================================saveAndColoringNetwork END===========================================");
    }

    public ColoringTopologicalOrderdMap getColoringTopologicalOrderdMap() {
        return coloringTopologicalOrderdMap;
    }

    private void doColoring(LearningFunctionInterface learning) {
        coloringTopologicalOrderdMap = new ColoringTopologicalOrderdMap(learning);
        coloringTopologicalOrderdMap.coloringMapWithTrainingData();
        coloringTopologicalOrderdMap.coloringFullMap();
        coloringTopologicalOrderdMap.printTopologyToConsole();
    }
}
