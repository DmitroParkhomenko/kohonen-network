package main;

import coloring.ColoringTopologicalOrderdMap;
import kohonen.LearningFunctionInterface;
import kohonen.WTMLearningFunction;
import metrics.EuclidesMetric;
import network.DefaultNetwork;
import document.core.File2VectorInterface;
import topology.GaussNeighbourhoodFunction;
import topology.MatrixTopology;

import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created by dmitro on 15.06.2014.
 */
public class WTMLearningBuilderByIteration {

    private static Logger _logger = LoggerFactory.getLogger(WTMLearningBuilderByIteration.class);
    private WTMLearningFunction learning;
    private ColoringTopologicalOrderdMap coloringTopologicalOrderdMap;

    public void buildNetwork(File2VectorInterface docsEncoderUtils, Settings settings) throws IOException {
        _logger.info("================================buildNetwork - started=======================================");

        MatrixTopology topology = new MatrixTopology(
                settings.getRowNumber(),
                settings.getColNumber(),
                settings.getRadius());

        Double[] maxWeight = new Double[docsEncoderUtils.getData(0).length];
        Arrays.fill(maxWeight, 1);

        DefaultNetwork network = new DefaultNetwork(docsEncoderUtils.getData(0).length, maxWeight, topology, settings);

        learning = new WTMLearningFunction(
                network,
                settings.getMaxIteration(),
                new EuclidesMetric(),
                docsEncoderUtils,
                settings.getFunctionalFactor(),
                new GaussNeighbourhoodFunction(settings.getRadius()));

        learning.learnByIterationCount();

        _logger.info("=========================================buildNetwork - ended================================");
    }

    public void saveAndColoringNetwork() throws IOException {
        _logger.info("=========================================saveAndColoringNetwork START===========================================");

        learning.getNetworkModelInterface().networkToFile("networkToFile.txt");
        doColoring(learning);

        _logger.info("=========================================saveAndColoringNetwork END===========================================");
    }

    public ColoringTopologicalOrderdMap getColoringTopologicalOrderdMap() {
        return coloringTopologicalOrderdMap;
    }

    private void doColoring(LearningFunctionInterface learning) {
        coloringTopologicalOrderdMap = new ColoringTopologicalOrderdMap(learning);
        coloringTopologicalOrderdMap.coloringMapWithTrainingData();
        coloringTopologicalOrderdMap.coloringFullMap();
        coloringTopologicalOrderdMap.printTopologyToConsole();
    }
}
