package main;

import coloring.ColoringTopologicalOrderdMap;
import kohonen.LearningFunctionInterface;
import kohonen.WTALearningFunction;
import metrics.EuclidesMetric;
import network.DefaultNetwork;
import document.core.File2VectorInterface;
import topology.MatrixTopology;

import java.io.IOException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created by dmitro on 15.06.2014.
 */
public class WTALearningBuilderByError {

    private WTALearningFunction learning;
    private static Logger _logger = LoggerFactory.getLogger(WTALearningBuilderByError.class);
    private  ColoringTopologicalOrderdMap coloringTopologicalOrderdMap;

    public void buildNetwork(File2VectorInterface docsEncoderUtils, Settings settings) throws IOException {
        _logger.info("================================buildNetwork - started=======================================");

        MatrixTopology topology = new MatrixTopology(
                settings.getRowNumber(),
                settings.getColNumber());

        int inputVectorLen = docsEncoderUtils.getData(0).length;
        Double[] maxWeight = new Double[inputVectorLen];
        Arrays.fill(maxWeight, 1.0);

        learning = new WTALearningFunction(
                new DefaultNetwork(inputVectorLen, maxWeight, topology, settings),
                new EuclidesMetric(),
                docsEncoderUtils,
                settings.getFunctionalFactor()
        );

        learning.setError(settings.getErrorLimit());
        learning.learnByMapError();

        _logger.info("=========================================buildNetwork - ended================================");
    }

    public void saveAndColoringNetwork() throws IOException {
        _logger.info("=========================================saveAndColoringNetwork START===========================================");

        learning.getNetworkModelInterface().networkToFile("networkToFile.txt");
        doColoring(learning);

        _logger.info("=========================================saveAndColoringNetwork END===========================================");
    }

    public ColoringTopologicalOrderdMap getColoringTopologicalOrderdMap() {
        return coloringTopologicalOrderdMap;
    }

    private void doColoring(LearningFunctionInterface learning) {
        coloringTopologicalOrderdMap = new ColoringTopologicalOrderdMap(learning);
        coloringTopologicalOrderdMap.coloringMapWithTrainingData();
        coloringTopologicalOrderdMap.coloringFullMap();
        coloringTopologicalOrderdMap.printTopologyToConsole();
    }
}
