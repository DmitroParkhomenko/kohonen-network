package main;

import coloring.ColoringTopologicalOrderdMap;
import efficient_initialization_scheme.InitializationScheme;
import kohonen.LearningFunctionInterface;
import kohonen.WTMLearningFunction;
import lvq.Cluster;
import lvq.LearningVectorQuantization;
import metrics.EuclidesMetric;
import network.DefaultNetwork;
import neuron.NeuronModel;
import document.core.File2VectorInterface;
import topology.GaussNeighbourhoodFunction;
import topology.MatrixTopology;

import java.io.IOException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.VectorComparator;

/**
 * Created by dmitro on 15.06.2014.
 */
public class WTMLearningBuilderWithInitSchemaByError {
    private static Logger _logger = LoggerFactory.getLogger(WTMLearningBuilderWithInitSchemaByError.class);
    private WTMLearningFunction learning;
    private ColoringTopologicalOrderdMap coloringTopologicalOrderdMap;

    public void buildNetwork(File2VectorInterface docsEncoderUtils, Settings settings) throws IOException {
        _logger.info("================================buildNetwork - started=======================================");

        LearningVectorQuantization lvq = new LearningVectorQuantization(settings.getThreshold());
        ArrayList<Cluster> clusters = lvq.run(docsEncoderUtils);

        MatrixTopology topology = new MatrixTopology(
                settings.getRowNumber(),
                settings.getColNumber(),
                settings.getRadius());

        InitializationScheme scheme = new InitializationScheme(new EuclidesMetric(), topology);
        NeuronModel[] neuronModels = scheme.run(clusters);

//        int eqVCount = VectorComparator.compare(neuronModels);

        learning = new WTMLearningFunction(
                new DefaultNetwork(neuronModels, topology),
                new EuclidesMetric(),
                docsEncoderUtils,
                settings.getFunctionalFactor(),
                new GaussNeighbourhoodFunction(settings.getRadius()));

        learning.setError(settings.getErrorLimit());
        learning.learnByMapError();

        _logger.info("=========================================buildNetwork - ended================================");
    }

    public void saveAndColoringNetwork() throws IOException {
        _logger.info("=========================================saveAndColoringNetwork START===========================================");

        learning.getNetworkModelInterface().networkToFile("networkToFile.txt");
        doColoring(learning);

        _logger.info("=========================================saveAndColoringNetwork END===========================================");
    }

    public ColoringTopologicalOrderdMap getColoringTopologicalOrderdMap() {
        return coloringTopologicalOrderdMap;
    }

    private void doColoring(LearningFunctionInterface learning) {
        coloringTopologicalOrderdMap = new ColoringTopologicalOrderdMap(learning);
        coloringTopologicalOrderdMap.coloringMapWithTrainingData();
        coloringTopologicalOrderdMap.coloringFullMap();
//        coloringTopologicalOrderdMap.printTopologyToConsole();
    }
}
