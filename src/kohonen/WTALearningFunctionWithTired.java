package kohonen;

import learning_factor_functional.LearningFactorFunctionalModel;
import metrics.MetricModel;
import network.NetworkModelInterface;
import neuron.TiredNeuronModel;

/**
 * WTALearningFunctionWithTired class - learnig class used to learnByIterationCount
 * neurons with tiredness
 */

/**
 * Created by dmitro on 11.05.2014.
 */

public class WTALearningFunctionWithTired extends WTALearningFunction {
    
     /**
     * Creates a new instance of WTALearningFunction.
     * @param networkModelInterface network model
     * @param maxIteration iteration number
     * @param metrics metrics
     * @param learningData learnig data
     * @param functionalModel functional model
     * @see MetricModel
     * @see LearningDataModelInterface
     * @see network.NetworkModelInterface
     * @see LearningFactorFunctionalModel
     */
    public WTALearningFunctionWithTired(
            NetworkModelInterface networkModelInterface,
            int maxIteration,
            MetricModel metrics,
            LearningDataModelInterface learningData,
            LearningFactorFunctionalModel functionalModel) {
        super(networkModelInterface,maxIteration,metrics,learningData,functionalModel);
    }
    
    
     /**
     * Return number of the best neuron for specified input vector.
     * All neuron's tiredness is increasing
     * @param vector input vector
     * @return Neuron number
     */
    public int getBestNeuron(Double[] vector) {
        int bestNeuron = super.getBestNeuron(vector);
        TiredNeuronModel tempNeuron;
        int networkSize = networkModelInterface.getNumbersOfNeurons();
        int tiredness;

        for(int i = 0; i < networkSize; i++) {
            tempNeuron = (TiredNeuronModel) networkModelInterface.getNeuron(i);
            tiredness = tempNeuron.getTiredness();
            tempNeuron.setTiredness(++tiredness);
        }

        return bestNeuron;
    }
    
    /**
     * Change neuron weights for specified neuron number,iteration and input data vector.
     * Changed neruon's tiredness is decreasing.
     * @param neuronNumber neuron number
     * @param vector input vector
     * @param iteration iteration number
     */
    
    protected void changeNeuronWeight(int neuronNumber, Double[] vector, int iteration) {
        super.changeNeuronWeight(neuronNumber,vector,iteration);
        
        TiredNeuronModel tempNeuron = (TiredNeuronModel) networkModelInterface.getNeuron(neuronNumber);
        int tiredness = tempNeuron.getTiredness();
        tempNeuron.setTiredness(tiredness - 2); 
    }
}
