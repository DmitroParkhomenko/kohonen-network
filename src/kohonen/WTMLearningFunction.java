package kohonen;

import learning_factor_functional.LearningFactorFunctionalModel;
import metrics.MetricModel;
import network.NetworkModelInterface;
import neuron.NeuronModel;
import topology.NeighbourhoodFunctionModel;
import topology.TopologyModel;

import java.util.TreeSet;

/**
 * <I>Winner Takes Most</I> - algorytm where winnig neuron and neurons in neighboorhood
 * weights are changed according to the formula w(k+1) = w(k) + n *N(i,x)* (x-w) where <br>
 * <I>w(k+1)</I> - neuron weight in <I>k +1</I> interation <br>
 * <I>w(k)</I> - neruon weight for <I>k</I> iteration <br>
 * <I>n</I> - value of learning function factor for <I> k </I> iteriation<br>
 * <i>N(i,x)</i> - value of neighboorhood function for <i>i </i> - specified neuron
 * <I>x</I> - learning vector od data
 * <I>w</I> - neuron weight
 */

/**
 * Created by dmitro on 11.05.2014.
 */
public class WTMLearningFunction extends CommonLearningFunction {
    /**
     * reference to topology model
     */
    protected TopologyModel topology;
    /**
     * reference to neighboorhood function model
     */
    protected NeighbourhoodFunctionModel neighboorhoodFunction;
    /**
     * <I>True</I> if comments during learning must be shown,<I> false</I> otherwise.
     */
    public WTMLearningFunction(NetworkModelInterface networkModelInterface,
                               int maxIteration,
                               MetricModel metrics,
                               LearningDataModelInterface learningData,
                               LearningFactorFunctionalModel functionalModel,
                               NeighbourhoodFunctionModel neighboorhoodFunction) {
        this.maxIteration = maxIteration;
        this.networkModelInterface = networkModelInterface;
        this.metrics = metrics;
        this.learningData = learningData;
        this.functionalModel = functionalModel;
        this.topology = networkModelInterface.getTopology();
        this.neighboorhoodFunction = neighboorhoodFunction;
    }

    public WTMLearningFunction(NetworkModelInterface networkModelInterface, MetricModel metrics,
                               LearningDataModelInterface learningData, LearningFactorFunctionalModel functionalModel,
                               NeighbourhoodFunctionModel neighboorhoodFunction) {
        this.networkModelInterface = networkModelInterface;
        this.metrics = metrics;
        this.learningData = learningData;
        this.functionalModel = functionalModel;
        this.topology = networkModelInterface.getTopology();
        this.neighboorhoodFunction = neighboorhoodFunction;
    }

    public void setError(Double errorLimit) {
        this.errorLimit = errorLimit;
    }

    /**
     * Rerturn number of best neuron for specified input vector
     * 
     * @param vector input vector
     * @return NeuronModelnumber
     */
    public int getBestNeuron(Double[] vector) {
        Double bestDistance = -1.0;
        int networkSize = networkModelInterface.getNumbersOfNeurons();
        int bestNeuron = 0;

        for(int i = 0; i < networkSize; i++) {
            NeuronModel tempNeuron = networkModelInterface.getNeuron(i);

            if(tempNeuron != null) {
                Double distance = metrics.getDistance(tempNeuron.getWeight(), vector);

                if(distance < bestDistance || bestDistance == -1) {
                    bestDistance = distance;
                    bestNeuron = i;
                }
            }
        }

        return bestNeuron;
    }
    
    /**
     * Change neuron weights for specified neuron number, iteration, input data vector and distance
     * and distance to winning neuron
     * @param distance distance to winning neuron
     * @param neuronNumber neuron number
     * @param trainingVector input vector
     * @param rate rate
     */
    protected void changeNeuronWeight(int neuronNumber, Double[] trainingVector, double rate, Double distance){
        Double[] weightList = networkModelInterface.getNeuron(neuronNumber).getWeight();

        Double neighboorhood = neighboorhoodFunction.getValue(distance);

        for (int i = 0; i < weightList.length; i++) {
            weightList[i] += rate * neighboorhood * (trainingVector[i] - weightList[i]);
        }

        networkModelInterface.getNeuron(neuronNumber).setWeight(weightList);
    }
    
    /**
     * Change specified neuron weight
     * @param bestMatchingUnitNeuronIndex neuron Number
     * @param trainingVector input trainingVector
     * @param rate rate
     */
    @Override
    public void changeWeight(int bestMatchingUnitNeuronIndex, Double[] trainingVector, double rate) {
        TreeSet<Integer> set = topology.neighbourhoodBuilder(bestMatchingUnitNeuronIndex);

        for (Integer neuron2Change : set) {
            Double neighbourhoodDistance = metrics.getDistance(
                    networkModelInterface.getNeuron(bestMatchingUnitNeuronIndex).getWeight(),
                    networkModelInterface.getNeuron(neuron2Change).getWeight());

            changeNeuronWeight(neuron2Change, trainingVector, rate, neighbourhoodDistance);
        }
    }
}
