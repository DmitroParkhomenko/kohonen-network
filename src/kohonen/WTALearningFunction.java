package kohonen;

import learning_factor_functional.LearningFactorFunctionalModel;
import metrics.MetricModel;
import network.NetworkModelInterface;
import neuron.NeuronModel;

/**
 * <I>Winner Takes All</I> - algorytm there only winnig neuron
 * weights are changed according to the formula w(k+1) = w(k) + n * (x-w) where <br>
 * <I>w(k+1)</I> - neuron weight in <I>k +1</I> interation <br>
 * <I>w(k)</I> - neruon weight for <I>k</I> iteration <br>
 * <I>n</I> - value of learning function factor for <I> k </I> iteriation<br>
 * <I>x</I> - learning vector od data
 * <I>w</I> - neuron weight
 */

/**
 * Created by dmitro on 11.05.2014.
 */
public class WTALearningFunction extends CommonLearningFunction {
    public WTALearningFunction(NetworkModelInterface networkModelInterface,
                               int maxIteration,
                               MetricModel metrics,
                               LearningDataModelInterface learningData,
                               LearningFactorFunctionalModel functionalModel) {
        this.maxIteration = maxIteration;
        this.networkModelInterface = networkModelInterface;
        this.metrics = metrics;
        this.learningData = learningData;
        this.functionalModel = functionalModel;
    }

    public WTALearningFunction(
            NetworkModelInterface networkModelInterface,
            MetricModel metrics,
            LearningDataModelInterface learningData,
            LearningFactorFunctionalModel functionalModel) {

        this.networkModelInterface = networkModelInterface;
        this.metrics = metrics;
        this.learningData = learningData;
        this.functionalModel = functionalModel;
    }

    public void setError(Double errorLimit) {
        this.errorLimit = errorLimit;
    }

    @Override
    public void changeWeight(int neuronNumber, Double[] vector, double rate) {
        changeNeuronWeight(neuronNumber, vector, rate);
    }

    /**
     * Return number of the best neuron for specified input vector
     *
     * @param vector input vector
     * @return Neuron number
     */
    public int getBestNeuron(Double[] vector) {
        NeuronModel tempNeuron;
        Double distance, bestDistance = -1.0;
        int networkSize = networkModelInterface.getNumbersOfNeurons();
        int bestNeuron = 0;

        for (int i = 0; i < networkSize; i++) {
            tempNeuron = networkModelInterface.getNeuron(i);
            distance = metrics.getDistance(tempNeuron.getWeight(), vector);

            if (distance < bestDistance || bestDistance == -1) {
                bestDistance = distance;
                bestNeuron = i;
            }
        }

        return bestNeuron;
    }

    /**
     * Change neuron weights for specified neuron number,iteration and input data vector
     *
     * @param neuronNumber neuron number
     * @param vector       input vector
     * @param rate    rate
     */
    protected void changeNeuronWeight(int neuronNumber, Double[] vector, double rate) {
        Double[] weighs = networkModelInterface.getNeuron(neuronNumber).getWeight();

        for (int i = 0; i < weighs.length; i++) {
            double tmp = weighs[i];
            weighs[i] += rate * (vector[i] - weighs[i]);

            if( Math.abs(vector[i] - weighs[i]) > Math.abs(tmp - vector[i]))
                System.out.println("!!!!!!!!!!!!!!!!!!!!!! ?????????????????????????????");
        }

        networkModelInterface.getNeuron(neuronNumber).setWeight(weighs);
    }
}