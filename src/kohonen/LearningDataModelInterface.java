package kohonen;

/**
 * Obiect containg learning data. Data are stored as array 
 * containing Double values.
 */

/**
 * Created by dmitro on 11.05.2014.
 */

public interface LearningDataModelInterface {
    Double[] getData(int index);
    String toString();
    int getDataSize();
}