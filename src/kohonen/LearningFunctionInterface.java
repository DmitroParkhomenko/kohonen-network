package kohonen;

import learning_factor_functional.LearningFactorFunctionalModel;
import metrics.MetricModel;
import network.NetworkModelInterface;
import topology.NeighbourhoodFunctionModel;

/**
 * Created by dmitro on 12.06.2014.
 */
public interface LearningFunctionInterface {

    void setNeighboorhoodFunction(NeighbourhoodFunctionModel neighboorhoodFunction);

    NeighbourhoodFunctionModel getNeighboorhoodFunction();

    MetricModel getMetrics();

    void setMetrics(MetricModel metrics);

    void setNetworkModelInterface(NetworkModelInterface networkModelInterface);

    NetworkModelInterface getNetworkModelInterface();

    void setMaxIteration(int maxIteration);

    int getMaxIteration();

    void setLearningData(LearningDataModelInterface learningData);

    LearningDataModelInterface getLearningData();

    void setFunctionalModel(LearningFactorFunctionalModel functionalModel);

    LearningFactorFunctionalModel getFunctionalModel();

    void learnByIterationCount();

    void learnByMapError();
}
