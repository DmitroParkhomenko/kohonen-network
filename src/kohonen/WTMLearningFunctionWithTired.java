package kohonen;

import learning_factor_functional.LearningFactorFunctionalModel;
import network.NetworkModelInterface;
import topology.NeighbourhoodFunctionModel;
import metrics.MetricModel;
import neuron.TiredNeuronModel;

/**
 * WTMLearningFunctionWithTired class - learnig class used to learnByIterationCount
 * neuron with tiredness
 */

/**
 * Created by dmitro on 11.05.2014.
 */
public class WTMLearningFunctionWithTired extends WTMLearningFunction {
    /**
     * Creates a new instance of WTMLearningFunction
     * @param networkModelInterface reference to network Model
     * @param maxIteration max number of iteration
     * @param metrics reference to metrics
     * @param learningData reference to learning data
     * @param functionalModel reference to functional Model
     * @param neighboorhoodFunction reference to Neighboorhood Function
     */
    public WTMLearningFunctionWithTired(NetworkModelInterface networkModelInterface,int maxIteration,MetricModel metrics,
            LearningDataModelInterface learningData,LearningFactorFunctionalModel functionalModel,
            NeighbourhoodFunctionModel neighboorhoodFunction) {
    super(networkModelInterface,maxIteration,metrics,learningData,functionalModel,
            neighboorhoodFunction);
    }
    
    
     /**
     * Return number of best neuron for specified input vector. 
     * Tiredness for all neurons increas
     * @param vector input vector
     * @return NeuronModelnumber
     */
    public int getBestNeuron(Double[] vector){
        int bestNeuron = super.getBestNeuron(vector);
        TiredNeuronModel tempNeuron;
        int networkSize = networkModelInterface.getNumbersOfNeurons();
        int tiredness;
        for(int i=0; i< networkSize; i++){
            tempNeuron = (TiredNeuronModel) networkModelInterface.getNeuron(i);
            tiredness = tempNeuron.getTiredness();
            tempNeuron.setTiredness(++tiredness);
        }
        return bestNeuron;
    }
    
    
    /**
     * Change neuron weights for specified neuron number, iteration, input data vector and distance
     * and distance to winning neuron. All changed neurons'
     * tiredness is decrease.
     * @param distance distance to winning neuron
     * @param neuronNumber neuron number
     * @param vector input vector
     * @param iteration iteration number
     */
    protected void changeNeuronWeight(int neuronNumber, Double[] vector, int iteration, double distance){
        super.changeNeuronWeight(neuronNumber, vector, iteration, distance);
        
        TiredNeuronModel tempNeuron = (TiredNeuronModel) networkModelInterface.getNeuron(neuronNumber);
        int tiredness = tempNeuron.getTiredness();
        tempNeuron.setTiredness(tiredness - 2); 
    }
}
