package kohonen;

import learning_factor_functional.LearningFactorFunctionalModel;
import metrics.MetricModel;
import network.NetworkModelInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import topology.NeighbourhoodFunctionModel;

/**
 * Created by dmitro on 16.12.2014.
 */
public abstract class CommonLearningFunction implements LearningFunctionInterface {

    private static Logger _logger = LoggerFactory.getLogger(CommonLearningFunction.class);

    protected Double errorLimit;

    /**
     * reference to metrics
     */
    protected MetricModel metrics;
    /**
     * reference to network model
     */
    protected NetworkModelInterface networkModelInterface;
    /**
     * max number of iteration
     */
    protected int maxIteration;
    /**
     * reference to learning data
     */
    protected LearningDataModelInterface learningData;
    /**
     * reference to function model
     */
    protected LearningFactorFunctionalModel functionalModel;

    protected abstract int getBestNeuron(Double[] vector);

    protected abstract void changeWeight(int neuronNumber, Double[] vector, double rate);

    public void learnByMapError() {
        int dataSize = learningData.getDataSize();
        int epoch = 0;
        Double error = 0.0;

        do {
            epoch++;

            Double summa = 0.0;

            Double rate = functionalModel.getValue(epoch);
//            System.out.println("rate = " + rate);
//            System.out.println("IT = " + epoch);

            for (int j = 0; j < dataSize; j++) {

                Double[] vector = learningData.getData(j);
                int bestNeuron = getBestNeuron(vector);

//                _logger.info("Best neuron number: " + (bestNeuron + 1));

                changeWeight(bestNeuron, vector, rate);
                Double distance = metrics.getDistance(networkModelInterface.getNeuron(bestNeuron).getWeight(), vector);

//                _logger.info("distance = " + distance);

                summa += distance;
            }

            error = summa / dataSize;

//            _logger.info("Current summa: " + summa);
            _logger.info("Current error: " + error);

//            if(epoch == 50) {
//                System.out.println("|||||||||||||||||EPOCH FINAL|||||||||||||||||||||||||");
//                break;
//            }

            if(rate == 0.0) {
                System.out.println("|||||||||||||||||rate is zero|||||||||||||||||||||||||");
                break;
            }

        } while (error > errorLimit);

        _logger.info("ITERATION COUNT = " + epoch);
    }

    /**
     * Start learning process
     */
    public void learnByIterationCount() {
        int dataSize = learningData.getDataSize();

        for (int iterationCounter = 0; iterationCounter < maxIteration; iterationCounter++) {

            _logger.info("ITERATION NUMBER = " + (iterationCounter + 1));

            for (int j = 0; j < dataSize; j++) {

                Double[] vector = learningData.getData(j);
                int bestNeuron = getBestNeuron(vector);

//                _logger.info("Best neuron number: " + (bestNeuron + 1));

                changeWeight(bestNeuron, vector, iterationCounter);
            }
        }
    }

    @Override
    public void setNeighboorhoodFunction(NeighbourhoodFunctionModel neighboorhoodFunction) {
        throw new UnsupportedOperationException();
    }

    @Override
    public NeighbourhoodFunctionModel getNeighboorhoodFunction() {
        throw new UnsupportedOperationException();
    }

    /**
     * Return metrics
     *
     * @return metrics
     * @see MetricModel
     */
    public MetricModel getMetrics() {
        return metrics;
    }

    /**
     * Set metrics
     *
     * @param metrics metrics
     */
    public void setMetrics(MetricModel metrics) {
        this.metrics = metrics;
    }

    /**
     * Set network model
     *
     * @param networkModelInterface network model
     */
    public void setNetworkModelInterface(NetworkModelInterface networkModelInterface) {
        this.networkModelInterface = networkModelInterface;
    }

    /**
     * Return network model
     *
     * @return network model
     */
    public NetworkModelInterface getNetworkModelInterface() {
        return networkModelInterface;
    }

    /**
     * Set max interation
     *
     * @param maxIteration max interation
     */
    public void setMaxIteration(int maxIteration) {
        this.maxIteration = maxIteration;
    }

    /**
     * Return maximal number of iteration
     *
     * @return maximal number of iteration
     */
    public int getMaxIteration() {
        return maxIteration;
    }

    /**
     * Set reference to learning data
     *
     * @param learningData reference to learning data
     */
    public void setLearningData(LearningDataModelInterface learningData) {
        this.learningData = learningData;
    }

    /**
     * Return reference to learning data
     *
     * @return reference to learning data
     */
    public LearningDataModelInterface getLearningData() {
        return learningData;
    }

    /**
     * Set functional learning factor model
     *
     * @param functionalModel functional learning factor model
     */
    public void setFunctionalModel(LearningFactorFunctionalModel functionalModel) {
        this.functionalModel = functionalModel;
    }

    /**
     * Return function model
     *
     * @return function model
     */
    public LearningFactorFunctionalModel getFunctionalModel() {
        return functionalModel;
    }
}
