package network;

import neuron.NeuronModel;
import topology.TopologyModel;

import java.io.IOException;

/**
 * Network Model interaface
 */
public interface NetworkModelInterface {

    /**
     * Return neuron with specified number
     * @param neuronNumber neuron number
     * @return neuron
     */
    NeuronModel getNeuron(int neuronNumber);
    
    /**
     * Return numbers of neurons
     * @return number of neuron
     */
    int getNumbersOfNeurons();
    
    /**
     * Return topology
     * @return topology model
     * @see TopologyModel
     */
    TopologyModel getTopology();
    
    /**
     * Set topology model
     * @param topology topology
     */
    void setTopology(TopologyModel topology);

    void networkToFile(String fileName) throws IOException;
}