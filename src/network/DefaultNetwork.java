package network;

import java.io.FileWriter;
import java.io.PrintWriter;

import main.Settings;
import neuron.KohonenNeuron;
import neuron.NeuronModel;
import topology.TopologyModel;

import java.text.DecimalFormat;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Default Network Model
 */
/**
 * Created by dmitro on 04.06.2014.
 */
public class DefaultNetwork implements NetworkModelInterface {

    private static Logger _logger = LoggerFactory.getLogger(DefaultNetwork.class);

    /**
     * Array of neurons
     */
    private NeuronModel[] neuronList;

    /**
     * Reference to topology
     */
    private TopologyModel topology;

    /**
     * Create network with specified topology, random weight from
     * definied interval and number of inputs
     *
     * @param weightNumber number of weights (inputs)
     * @param maxWeight    array with specified weight
     * @param topology     Topology
     */
    public DefaultNetwork(int weightNumber, Double[] maxWeight, TopologyModel topology, Settings settings) {
        this.topology = topology;
        int numberOfNeurons = topology.getNumbersOfNeurons();
        neuronList = new KohonenNeuron[numberOfNeurons];

        for (int i = 0; i < numberOfNeurons; i++) {
            neuronList[i] = new KohonenNeuron(weightNumber, maxWeight, settings);
        }
    }

    public  DefaultNetwork(NeuronModel[] neuronList, TopologyModel topology) {
        this.topology = topology;
        this.neuronList = neuronList;
    }

    /**
     * Create network with specified topology and parameters get from
     * specified file
     *
     * @param fileName File Name
     * @param topology Topology
     */
    public DefaultNetwork(String fileName, TopologyModel topology) {

        int neuronNumber = topology.getNumbersOfNeurons();
        neuronList = new KohonenNeuron[neuronNumber];
        String[] tempTable;
        Double[] tempList;
        int rows = 0;

        try (
                FileReader fr = new FileReader(new File(fileName));
                BufferedReader input = new BufferedReader(fr);
        ) {

            String line;

            _logger.info("Data from: \"" + fileName + "\" are importing...");

            while ((line = input.readLine()) != null) {
                tempTable = line.split("\t");
                int tableLenght = tempTable.length;
                tempList = new Double[tableLenght];
                for (int i = 0; i < tableLenght; i++) {
                    tempList[i] = Double.valueOf(tempTable[i]);
                }
                neuronList[rows] = new KohonenNeuron(tempList);
                rows++;
            }
            fr.close();

            _logger.info(rows + " rows was imported");
        } catch (IOException e) {
            _logger.info("File can not be read!. Error: " + e);
        }

        this.topology = topology;
    }

    /**
     * Return specified by number neuron
     *
     * @param neuronNumber neuron number
     * @return Neuorn
     */
    public NeuronModel getNeuron(int neuronNumber) {
        return neuronList[neuronNumber];
    }

    /**
     * Return number of neuorns
     *
     * @return nmber of neurons
     */
    public int getNumbersOfNeurons() {
        return neuronList.length;
    }

    /**
     * Get topology reference
     *
     * @return Topology
     */
    public TopologyModel getTopology() {
        return topology;
    }

    /**
     * Set topology
     *
     * @param topology Topology
     */
    public void setTopology(TopologyModel topology) {
        this.topology = topology;
    }

    /**
     * Returns a string representation of the Coords object
     *
     * @return Returns a string representation of the Coords object
     */
    public String toString() {
        String text = "";
        int networkSize = neuronList.length;
        for (int i = 0; i < networkSize; i++) {
            text += "Neuron number " + (i + 1) + ": " + neuronList[i];
            if (i < networkSize - 1) {
                text += "\n";
            }
        }
        return text;
    }

    /**
     * Save network into file
     *
     * @param fileName File Name
     */
    public void networkToFile(String fileName) throws IOException {
        Double[] weight;
        DecimalFormat df = new DecimalFormat("0.00000");

        try (
              FileWriter fw = new FileWriter(new File(fileName));
              PrintWriter pw = new PrintWriter(fw);
        ) {
            int networkSize = neuronList.length;

            for (int index = 0; index < networkSize; index++) {
                StringBuilder weightList = new StringBuilder("");
                weight = neuronList[index].getWeight();

                for (int j = 0; j < weight.length; j++) {
                    weightList.append(df.format(weight[j]));
                    if (j < weight.length - 1) {
                        weightList.append("\t");
                    }
                }

                pw.println(weightList);
            }
        }
    }
}