package efficient_initialization_scheme;

import lvq.Cluster;
import main.Settings;
import metrics.MetricModel;
import neuron.KohonenNeuron;
import neuron.NeuronModel;
import topology.MatrixTopology;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitro on 31.05.2014.
 */
public class InitializationScheme {
    private MetricModel metricModel;
    private MatrixTopology topology;
    private NeuronModel[][] neuronMatrix;

    public InitializationScheme(MetricModel metricModel, MatrixTopology topology) {
        this.metricModel = metricModel;
        this.topology = topology;
    }

    public NeuronModel[] run(final ArrayList<Cluster> cluster) {
        createNeuronMatrix();
        initNeuronsOnCorners(cluster);
        initNeuronsOnEdges();
        initAllNeuronsExceptEdges();
        return convertMatrixToVector();
    }

    private void createNeuronMatrix() {
        neuronMatrix = new NeuronModel[topology.getRowNumber()][topology.getColNumber()];

        for(int i = 0; i < topology.getRowNumber(); i++) {
            for(int j = 0; j < topology.getColNumber(); j++) {
//                neuronMatrix[i][j] = new KohonenNeuron(0, null, new Settings());
                neuronMatrix[i][j] = new KohonenNeuron();
            }
        }
    }

    private NeuronModel[] convertMatrixToVector() {
        NeuronModel[] neuronList = new NeuronModel[topology.getRowNumber() * topology.getColNumber()];

        for (int i = 0; i < topology.getRowNumber(); i++) {
            for (int j = 0; j < topology.getColNumber(); j++) {
                neuronList[topology.getColNumber() * i + j] = neuronMatrix[i][j];
            }
        }

        return neuronList;
    }

    //step 1
    private void initNeuronsOnCorners(ArrayList<Cluster> cluster) {
        List<Integer> twoMaxDistVectors = getTwoMaxDistVectors(cluster);
        int clusterIndexFirst = twoMaxDistVectors.get(0);
        int clusterIndexSecond = twoMaxDistVectors.get(1);
        int clusterIndexThird = getOneMostDistFromTwoVectors(cluster, clusterIndexFirst, clusterIndexSecond);
        int clusterIndexFourth = getOneMaxDistToFourVectors(cluster, clusterIndexFirst, clusterIndexSecond, clusterIndexThird);

        Double[] upperRight = cluster.get(clusterIndexFirst).getmCenter();
        Double[] bottomLeft = cluster.get(clusterIndexSecond).getmCenter();
        Double[] bottomRight = cluster.get(clusterIndexThird).getmCenter();
        Double[] upperLeft = cluster.get(clusterIndexFourth).getmCenter();

        int lastColl = topology.getColNumber() - 1;
        int lastRow = topology.getRowNumber() - 1;

        neuronMatrix[0][0].setWeight(upperLeft);
        neuronMatrix[0][lastColl].setWeight(upperRight);
        neuronMatrix[lastRow][0].setWeight(bottomLeft);
        neuronMatrix[lastRow][lastColl].setWeight(bottomRight);
    }

    private List<Integer> getTwoMaxDistVectors(ArrayList<Cluster> cluster) {
        Double maxDist = 0.0;
        int frtVecIndex = 0;
        int sectVecIndex = 0;

        for (int index = 0; index < cluster.size(); index++) {
            Double[] currVec1 = cluster.get(index).getmCenter();
            for (int index2 = index + 1; index2 < cluster.size(); index2++) {
                Double[] currVec2 = cluster.get(index2).getmCenter();
                Double currDist = metricModel.getDistance(currVec1, currVec2);
                if(currDist > maxDist) {
                    maxDist = currDist;
                    frtVecIndex = index;
                    sectVecIndex = index2;
                }
            }
        }

        List<Integer> result = new ArrayList<Integer>();
        result.add(frtVecIndex);
        result.add(sectVecIndex);
        return result;
    }

    private int getOneMostDistFromTwoVectors(ArrayList<Cluster> cluster, int frtVecIndex, int sectVecIndex) {
        Double maxDist1 = 0.0;
        Double maxDist2 = 0.0;
        int thdVecIndex = 0;

        for (int index = 0; index < cluster.size(); index++) {
            Double[] currVec = cluster.get(index).getmCenter();
            Double[] vec1 = cluster.get(frtVecIndex).getmCenter();
            Double[] vec2 = cluster.get(sectVecIndex).getmCenter();

            Double currDist1 = metricModel.getDistance(currVec, vec1);
            Double currDist2 = metricModel.getDistance(currVec, vec2);

            if(currDist1 > maxDist1 && currDist2 > maxDist2) {
                maxDist1 = currDist1;
                maxDist2 = currDist2;
                thdVecIndex = index;
            }
        }

        return thdVecIndex;
    }

    private int getOneMaxDistToFourVectors(ArrayList<Cluster> cluster, int frtVecIndex, int sectVecIndex, int thdVecIndex) {
        Double maxDist11 = 0.0;
        Double maxDist22 = 0.0;
        Double maxDist33 = 0.0;
        int forthVecIndex = 0;

        for (int index = 0; index < cluster.size(); index++) {
            Double[] currVec = cluster.get(index).getmCenter();
            Double[] vec1 = cluster.get(frtVecIndex).getmCenter();
            Double[] vec2 = cluster.get(sectVecIndex).getmCenter();
            Double[] vec3 = cluster.get(thdVecIndex).getmCenter();

            Double currDist1 = metricModel.getDistance(currVec, vec1);
            Double currDist2 = metricModel.getDistance(currVec, vec2);
            Double currDist3 = metricModel.getDistance(currVec, vec3);

            if(currDist1 > maxDist11 && currDist2 > maxDist22 && currDist3 > maxDist33) {
                maxDist11 = currDist1;
                maxDist22 = currDist2;
                maxDist33 = currDist3;
                forthVecIndex = index;
            }
        }

        return forthVecIndex;
    }

    //step 2
    private void initNeuronsOnEdges() {
        initTopEdge();
        initLeftEdge();
        initRightEdge();
        initBottomEdge();
    }

    //step 3
    private void initAllNeuronsExceptEdges() {
        final int ROW_NUM = topology.getRowNumber() - 1;
        final int COL_NUM = topology.getColNumber() - 1;

        for (int rowIndex = 1; rowIndex < ROW_NUM; rowIndex++) {
            for (int colIndex = 1; colIndex < COL_NUM; colIndex++) {

                Double coefficient1 = (colIndex - 1) / (double) COL_NUM;
                Double[] vec1 = neuronMatrix[rowIndex][COL_NUM].getWeight();
                multiplyVectorByCoefficient(vec1, coefficient1);

                Double coefficient2 = (topology.getColNumber() - colIndex) / (double)(COL_NUM);
                Double[] vec2 = neuronMatrix[rowIndex][0].getWeight();
                multiplyVectorByCoefficient(vec2, coefficient2);

                Double[] result = plusVectors(vec1, vec2);
                neuronMatrix[rowIndex][colIndex].setWeight(result);
            }
        }
    }

    private void initTopEdge() {
        final int COL_NUM = topology.getColNumber() - 1;
        Double[] upperLeftNeuronWeights = neuronMatrix[0][0].getWeight();
        Double[] upperRightNeuronWeights = neuronMatrix[0][COL_NUM].getWeight();

        for (int colIndex = 1; colIndex < COL_NUM; colIndex++) {

            Double coefficient1 = (colIndex - 1) / (double)(COL_NUM);
            multiplyVectorByCoefficient(upperLeftNeuronWeights, coefficient1);

            Double coefficient2 = (topology.getColNumber() - colIndex) / (double)(COL_NUM);
            multiplyVectorByCoefficient(upperRightNeuronWeights, coefficient2);

            Double[] result = plusVectors(upperLeftNeuronWeights, upperRightNeuronWeights);
            neuronMatrix[0][colIndex].setWeight(result);
        }
    }

    private void initBottomEdge() {
        final int ROW_NUM = topology.getRowNumber() - 1;
        final int COL_NUM = topology.getColNumber() - 1;
        Double[] bottomLeftNeuronWeights = neuronMatrix[ROW_NUM][0].getWeight();
        Double[] bottomRightNeuronWeights = neuronMatrix[ROW_NUM][COL_NUM].getWeight();

        for (int colIndex = 1; colIndex < COL_NUM; colIndex++) {

            Double cof1 = (colIndex - 1) / (double) (COL_NUM);
            multiplyVectorByCoefficient(bottomRightNeuronWeights, cof1);

            Double cof2 = (topology.getColNumber() - colIndex) / (double)(COL_NUM);
            multiplyVectorByCoefficient(bottomLeftNeuronWeights, cof2);

            Double[] result = plusVectors(bottomLeftNeuronWeights, bottomRightNeuronWeights);
            neuronMatrix[ROW_NUM][colIndex].setWeight(result);
        }
    }

    private void initRightEdge() {
        final int ROW_NUM = topology.getRowNumber() - 1;
        final int COL_NUM = topology.getColNumber() - 1;

        Double[] upperRightNeuronWeights = neuronMatrix[0][COL_NUM].getWeight();
        Double[] bottomRightNeuronWeights = neuronMatrix[ROW_NUM][COL_NUM].getWeight();

        for (int rowIndex = 1; rowIndex < ROW_NUM; rowIndex++) {

            Double cof1 = (rowIndex - 1) / (double)(ROW_NUM);
            multiplyVectorByCoefficient(bottomRightNeuronWeights, cof1);

            Double cof2 = (topology.getRowNumber() - rowIndex) / (double)(ROW_NUM);
            multiplyVectorByCoefficient(upperRightNeuronWeights, cof2);

            Double[] result = plusVectors(upperRightNeuronWeights, bottomRightNeuronWeights);
            neuronMatrix[rowIndex][COL_NUM].setWeight(result);
        }
    }

    private void initLeftEdge() {
        final int ROW_NUM = topology.getRowNumber() - 1;
        Double[] upperLeftNeuronWeights = neuronMatrix[0][0].getWeight();
        Double[] bottomLeftNeuronWeights = neuronMatrix[ROW_NUM][0].getWeight();

        for (int rowIndex = 1; rowIndex < ROW_NUM; rowIndex++) {

            Double coefficient1 = (rowIndex - 1) / (double)(ROW_NUM);
            multiplyVectorByCoefficient(bottomLeftNeuronWeights, coefficient1);

            Double coefficient2 = (topology.getRowNumber() - rowIndex) / (double)(ROW_NUM);
            multiplyVectorByCoefficient(upperLeftNeuronWeights, coefficient2);

            Double[] result = plusVectors(upperLeftNeuronWeights, bottomLeftNeuronWeights);
            neuronMatrix[rowIndex][0].setWeight(result);
        }
    }

    private Double[] plusVectors(Double[] vec1, Double[] vec2) {
        Double[] result = new Double[vec2.length];

        for (int index = 0; index < vec2.length; index++) {
            result[index] = vec1[index] + vec2[index];
        }

        return result;
    }

    private void multiplyVectorByCoefficient(Double[] vec, Double cof) {
        for (int index = 0; index < vec.length; index++) {
            vec[index] *= cof;
        }
    }
}
