package document.linear;

import document.core.*;
import document.stems.YandexMyStem;
import main.Consts;
import main.Filter;

import java.io.File;
import java.io.IOException;
import java.util.List;
/**
 * Created by dmitro on 11.05.2014.
 */

public class Runner {
    public void run() {
        final int MAX_WORDS_FROM_FILE = 10;
        DictionaryBridge dictionaryBuilder = new DictionaryBridge(Consts.DICTIONARY_PATH);
        Filter filter = new Filter();
        //loop for dictionary creation
        for (File file : filter.finder(Consts.LEAN_FOLDER_NAME)) {
            String path = file.getAbsolutePath();
            StemInterface stemInterface = new YandexMyStem(path);
            File2WordsCollection firstWordsStrategy = new File2WordsCollection(stemInterface, MAX_WORDS_FROM_FILE);
            List<Word> items = firstWordsStrategy.parseFile();
            try {
                dictionaryBuilder.push(items);
            } catch (IOException e) {
                e.printStackTrace();
                dictionaryBuilder.closeAll();
                break;
            }
        }
    }
}