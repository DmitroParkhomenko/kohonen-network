package document.concurrency.dictionary;

import document.core.Word;
import document.core.File2WordsCollection;
import document.core.StemInterface;
import document.stems.YandexMyStem;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by dmitro on 08.03.2015.
 */

public class Producer implements Runnable {
    private BlockingQueue<List<Word>> queue;
    private StemInterface stemInterface;
    private int maxWordsFromFile;

    public Producer(BlockingQueue<List<Word>> queue, StemInterface stemInterface, int maxWordsFromFile) {
        this.queue = queue;
        this.stemInterface = stemInterface;
        this.maxWordsFromFile = maxWordsFromFile;
    }

    @Override
    public void run() {
        File2WordsCollection firstWordsStrategy = new File2WordsCollection(stemInterface, maxWordsFromFile);
        List<Word> items = firstWordsStrategy.parseFile();

        try {
            queue.put(items);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            firstWordsStrategy.closeAll();
        }
    }
}
