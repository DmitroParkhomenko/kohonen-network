package document.concurrency.dictionary;

import document.core.StemInterface;
import document.core.Word;
import document.stems.UkrStemWrapper;
import main.Consts;
import main.Filter;

import java.io.File;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by dmitro on 08.03.2015.
 */

//Create dictionary
public class DictionaryRunner {
    private String filesForDictionaryFolderPath;
    private String dictionaryFilePath;
    private int maxWords;

    public DictionaryRunner(String filesForDictionaryFolderPath, String dictionaryFilePath, int maxWords) {
        this.filesForDictionaryFolderPath = filesForDictionaryFolderPath;
        this.dictionaryFilePath = dictionaryFilePath;
        this.maxWords = maxWords;
    }

    public DictionaryRunner() {
        this.filesForDictionaryFolderPath = Consts.LEAN_FOLDER_NAME;
        this.dictionaryFilePath =  Consts.DICTIONARY_PATH;
    }

    public void run() {
        int queueLimit = 1024;
        BlockingQueue<List<Word>> queue = new ArrayBlockingQueue<List<Word>>(queueLimit);
        int producerCount = 0;
        Filter filter = new Filter();
        for (File file : filter.finder(filesForDictionaryFolderPath)) {
            String path = file.getAbsolutePath();
            StemInterface stemInterface = new UkrStemWrapper(path);
//            StemInterface stemInterface = new YandexMyStem(path);
            Producer producer = new Producer(queue, stemInterface, maxWords);

            new Thread(producer).start();
            producerCount++;
        }

        Consumer consumer = new Consumer(queue, dictionaryFilePath, producerCount);
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();

        try {
            consumerThread.join();// block main Thread until the running consumerThread is not done
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}