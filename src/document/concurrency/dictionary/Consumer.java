package document.concurrency.dictionary;

import document.core.DictionaryBridge;
import document.core.Word;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;

/**
 * Created by dmitro on 08.03.2015.
 */
public class Consumer implements Runnable {
    private BlockingQueue<List<Word>> queue;
    private String path;
    private final int totalProducerItems;

    public Consumer(BlockingQueue<List<Word>> queue, String path, int totalProducerItems) {
        this.queue = queue;
        this.path = path;
        this.totalProducerItems = totalProducerItems;
    }

    @Override
    public void run() {
        Map<String, Integer> duplicateFilterMap = new HashMap<>(); //DB SELECT query simulation for checking duplication words in dictionary
        Map<String, Word> stringWordHashMap = new HashMap<>();

        if(queue == null || totalProducerItems <= 0 || path.isEmpty()) {
            return;
        }

        DictionaryBridge dictionaryBridge = new DictionaryBridge(path);
        dictionaryBridge.clear();
        dictionaryBridge.create();//read comment in DictionaryBridge class

        try {
            for (int i = 0; i < totalProducerItems; i++) {
                List<Word> items = queue.take();

                for (Word word : items) {
                    String value = word.getValue();

                    if (duplicateFilterMap.containsKey(value)) {
                        int countInAllFiles = duplicateFilterMap.get(value);
                        countInAllFiles++;
                        word.setWeight(Math.round(1 / (double) countInAllFiles * 100.0) / 100.0);

                        duplicateFilterMap.put(value, countInAllFiles);//updated count
                    } else {
                        word.setWeight(1);
                        duplicateFilterMap.put(value, 1);//start count
                    }

                    stringWordHashMap.put(value, word);
                }
            }

            List<Word> allWordsList = new ArrayList<>(stringWordHashMap.values());
            dictionaryBridge.push(allWordsList);//push all at once

        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        } finally {
            dictionaryBridge.closeAll();
        }
    }
}
