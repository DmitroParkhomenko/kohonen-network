package document.concurrency.file_processing;

import document.core.File2WordsCollection;
import document.core.StemInterface;
import document.core.String2BinaryLiteral;
import document.core.Word;

import java.util.*;
import java.util.concurrent.BlockingQueue;

/**
 * Created by dmitro on 09.03.2015.
 */
public class Consumer implements Runnable {
    private BlockingQueue<Word> queue;
    private StemInterface stemInterface;
    private Map<String, Double[]> synchronizedMap;
    private int maxWordsFromFile;

    public Consumer(BlockingQueue<Word> queue,
                    Map<String, Double[]> synchronizedMap,
                    StemInterface stemInterface,
                    int maxWordsFromFile) {
        this.queue = queue;
        this.stemInterface = stemInterface;
        this.synchronizedMap = synchronizedMap;
        this.maxWordsFromFile = maxWordsFromFile;
    }

    @Override
    public void run() {
        List<Double> result = new ArrayList<>();
        File2WordsCollection firstWordsStrategy = new File2WordsCollection(stemInterface, maxWordsFromFile);
        List<Word> wordList = firstWordsStrategy.parseFile();
        Map<String, Word> wordMap = new HashMap<>(wordList.size());

        for(Word word: wordList) {
            wordMap.put(word.getValue(), word);
        }

        firstWordsStrategy.closeAll();

        while (true) {
            Word dictionaryWord;

            try {
                dictionaryWord = queue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
                continue;//can I do that????
            }

            if (dictionaryWord.getValue().equals("STOP_PILL")) {
                break;
            }

            if (dictionaryWord.getBinaryLiteral() == 0) {
                Word word = wordMap.get(dictionaryWord.getValue());
                result.add(word != null ? word.getWeight() * dictionaryWord.getWeight() : 0.0);
            } else {
                int dictionaryLength = dictionaryWord.getValue().length();
                int binaryLiteral = dictionaryWord.getBinaryLiteral();
                boolean notFoundFlag = true;

                for (String wordValue : wordMap.keySet()) {
                    Word word = wordMap.get(wordValue);

                    if (word.getBinaryLiteral() == 0) {
                        continue;
                    }

                    int errorsCount = String2BinaryLiteral.compare(word.getBinaryLiteral(), binaryLiteral);
                    int length = word.getValue().length();

                    if (errorsCount < Math.min(length, dictionaryLength) / 2) {
                        result.add(word.getWeight() * dictionaryWord.getWeight());
                        notFoundFlag = false;
                        break;
                    }
                }

                if (notFoundFlag) {
                    result.add(0.0);
                }
            }
        }

        synchronizedMap.put(stemInterface.getFileName(), result.toArray(new Double[result.size()]));
    }
}