package document.concurrency.file_processing;

import java.util.Map;

/**
 * Created by dmitro on 12.03.2015.
 */
public interface ClusterizationInterface {
    Map<String, Double[]> geResults();
    void run();
}
