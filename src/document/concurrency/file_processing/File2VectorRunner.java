package document.concurrency.file_processing;

import document.core.File2VectorInterface;
import document.core.StemInterface;
import document.core.Word;
import document.stems.UkrStemWrapper;
import main.Consts;
import main.Filter;

import java.io.File;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by dmitro on 09.03.2015.
 */
public class File2VectorRunner implements File2VectorInterface, ClusterizationInterface {
    private Map<String, Double[]> map = Collections.synchronizedMap(new HashMap<String, Double[]>());
    private List<Double[]> list;
    private String trainingFolderPath;
    private String dictionaryFilePath;
    private int maxWords;

    public File2VectorRunner(String trainingFolderPath, String dictionaryFilePath, int maxWords) {
        this.trainingFolderPath = trainingFolderPath;
        this.dictionaryFilePath = dictionaryFilePath;
        this.maxWords = maxWords;
    }

    public File2VectorRunner() {
        trainingFolderPath = Consts.LEAN_FOLDER_NAME;
        dictionaryFilePath = Consts.DICTIONARY_PATH;
    }

    @Override
    public void run() {
        int queueLimit = 1024;
        List<BlockingQueue<Word>> blockingQueues = new ArrayList<>();
        Filter filter = new Filter();
        List<Thread> consumerThreadPool = new ArrayList<>();

        for (File file : filter.finder(trainingFolderPath)) {
            BlockingQueue<Word> queue = new ArrayBlockingQueue<>(queueLimit);
            blockingQueues.add(queue);

            String filePath = file.getAbsolutePath();
            StemInterface stemInterface = new UkrStemWrapper(filePath);
//            StemInterface stemInterface = new YandexMyStem(filePath);
            Consumer consumer = new Consumer(queue, map, stemInterface, maxWords);

            Thread consumerThread = new Thread(consumer);
            consumerThreadPool.add(consumerThread);

            consumerThread.start();
        }

        Producer producer = new Producer(blockingQueues, dictionaryFilePath);
        Thread producerThread = new Thread(producer);
        producerThread.start();

        for (Thread consumerThread : consumerThreadPool) {
            try {
                consumerThread.join();//wait in main Thread until all threads will die
            } catch (InterruptedException e) {
                System.out.println("InterruptedException under join");
                e.printStackTrace();
            }
        }

        list = new ArrayList<>(map.values());
    }

    @Override
    public int getDataSize() {
        return list.size();
    }

    @Override
    public List<Double[]> getDataList() {
        return list;
    }

    @Override
    public Double[] getData(int index) {
        return list.get(index);
    }

    @Override
    public Map<String, Double[]> geResults() {
        return map;
    }
}