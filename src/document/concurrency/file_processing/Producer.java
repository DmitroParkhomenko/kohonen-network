package document.concurrency.file_processing;

import document.core.DictionaryBridge;
import document.core.Word;

import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by dmitro on 09.03.2015.
 */
public class Producer implements Runnable {
    private List<BlockingQueue<Word>> queueList;
    private String dictionaryPath;

    public Producer(List<BlockingQueue<Word>> queueList, String dictionaryPath) {
        this.dictionaryPath = dictionaryPath;
        this.queueList = queueList;
    }

    @Override
    public void run() {
        DictionaryBridge dictionaryBridge = new DictionaryBridge(dictionaryPath);
        dictionaryBridge.read();

        try{
            while (dictionaryBridge.hasNext()) {
                Word word = dictionaryBridge.next();
                for(BlockingQueue<Word> blockingQueue: queueList) {
//                    System.out.println("P: put word");
                    blockingQueue.put(word);
                }
            }

            for(BlockingQueue<Word> blockingQueue: queueList) {
//                System.out.println("P: put STOP PILL word");
                Word stopPill = new Word();
                stopPill.setValue("STOP_PILL");
                blockingQueue.put(stopPill);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
//            System.out.println("P: end work");
            dictionaryBridge.closeAll();
        }
    }
}
