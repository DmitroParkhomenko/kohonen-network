package document.stems;

import document.core.StemInterface;
import main.Consts;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Scanner;

/**
 * Created by dmitro on 08.03.2015.
 */
public class YandexMyStem implements StemInterface {
    private Scanner scanner;
    private Process pr;
    private String path;

    public YandexMyStem(String path) {
        this.path = path;
        this.pr = builderProcess(path);
    }

    @Override
    public Scanner getScanner() {
        InputStreamReader streamReader = new InputStreamReader(pr.getInputStream());
        scanner = new Scanner(streamReader).useDelimiter(Consts.DELIMETR_FOR_MYSTEM_TOKENS);
        return scanner;
    }

    @Override
    public String extractWordFromToken(String currToken) {
        if(checkSpeechPart(currToken)) {
            return currToken.split("=")[0].substring(1);
        }

        return "";
    }

    @Override
    public void closeAll() {
        scanner.close();
    }

    @Override
    public String getFileName() {
        return path;
    }

    private Process builderProcess(String fileName) {
        Process process = null;

        try {
            String[] cmd = buildCommandForMyStrem(fileName);
            Runtime run = Runtime.getRuntime();
            process = run.exec(cmd);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return process;
    }

    private boolean checkSpeechPart(String currToken) {
        for (String speechPart : Consts.SPEECH_PART) {
            if (currToken.contains(speechPart)) {
                return true;
            }
        }

        return false;
    }

    private String[] buildCommandForMyStrem(String fileName) throws UnsupportedEncodingException {
        return new String[]{
                Consts.MYSTEM_PROGRAM,
                URLEncoder.encode(Consts.PARAMS, Consts.FILE_ENC),
                Consts.ENC_PATAM,
                fileName
        };
    }
}