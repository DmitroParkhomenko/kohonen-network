package document.stems;

import document.core.StemInterface;
import ua.stemmer.UkrainianStem;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by dmitro on 10.03.2015.
 */
public class UkrStemWrapper implements StemInterface {
    private String path;
    private Scanner scanner = null;

    public UkrStemWrapper(String path) {
        this.path =  path;
    }

    @Override
    public Scanner getScanner() {
        UkrainianStem ukrainianStem = new UkrainianStem(path);

        try {
            List<String> stringList = ukrainianStem.run();
            StringBuilder builder = new StringBuilder(stringList.size());

            for (String next : stringList) {
                builder.append(next);
                builder.append(" ");
            }

            builder.setLength(builder.length() - 1);

            scanner = new Scanner(builder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return scanner;
    }

    @Override
    public String getFileName() {
        return path;
    }

    @Override
    public String extractWordFromToken(String currToken) {
        return currToken.split("_")[0];
    }

    @Override
    public void closeAll() {
        if (scanner != null) {
            scanner.close();
        }
    }
}
