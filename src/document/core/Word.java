package document.core;

/**
 * Created by dmitro on 14.06.2014.
 */
public class Word {
    private String value;
    private int count;
    private int binaryLiteral;
    private String speechPart;
    private double weight;

    public Word() {
        value = "";
        count = 0;
        binaryLiteral = 0;
        speechPart = "";
        weight = 0;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getBinaryLiteral() {
        return binaryLiteral;
    }

    public void setBinaryLiteral(int binaryLiteral) {
        this.binaryLiteral = binaryLiteral;
    }

    public String getSpeechPart() {
        return speechPart;
    }

    public void setSpeechPart(String speechPart) {
        this.speechPart = speechPart;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word item = (Word) o;

        if (value != null ? !value.equals(item.value) : item.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        String part = speechPart.isEmpty() ? String.valueOf(binaryLiteral) : speechPart;
        return value + "_" + part + "_" + weight;
    }

    public static Word toWord(String strWordFormat) {
        String[] attributes = strWordFormat.split("_");

        if(attributes.length != 3) {
            return null;
        }

        Word word = new Word();
        word.setValue(attributes[0]);

        //Apache Commons Lang can be used for checking if a attributes[1] string is a numeric type
        if(attributes[1].matches("\\d+")) {
            word.setBinaryLiteral(Integer.parseInt(attributes[1]));
        } else {
            word.setSpeechPart(attributes[1]);
        }

        word.setWeight(Double.parseDouble(attributes[2]));

        return word;
    }
}
