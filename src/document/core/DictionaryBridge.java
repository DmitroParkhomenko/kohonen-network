package document.core;

import main.Consts;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by dmitro on 08.03.2015.
 */

//This class is a bridge between all app and db (At the moment it is a simple file, but late it will be a MongoDb or something similar)
public class DictionaryBridge implements DictionaryBridgeInterface {
    private BufferedWriter writer;
    private Scanner reader;
    private String path;

    public DictionaryBridge(String path) {
        this.path = path;
    }

    @Override
    public void create() {
        writer = getWriter(path);
    }

    @Override
    public void read() {
        reader = getReader(path);
    }

    @Override
    public boolean hasNext() {
        return reader.hasNext();
    }

    @Override
    public Word next() {
        return Word.toWord(reader.next());
    }

    @Override
    public void push(List<Word> itemList) throws IOException {
        for (Word item : itemList) {
            writer.write(item.toString());
            writer.newLine();
        }
    }

    @Override
    public void clear() {
        try(PrintWriter writer = new PrintWriter(path)) {}
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void closeAll() {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (reader != null) {
            reader.close();
        }
    }

    private BufferedWriter getWriter(String dictionaryFilePath) {
        BufferedWriter writer = null;

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(dictionaryFilePath, true);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, Consts.FILE_ENC);
            writer = new BufferedWriter(outputStreamWriter);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return writer;
    }

    private Scanner getReader(String dictionaryFilePath) {
        Scanner scanner = null;

        try {
            FileInputStream fileInputStream = new FileInputStream(dictionaryFilePath);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, Consts.FILE_ENC);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String newLine = System.getProperty("line.separator");
            scanner = new Scanner(bufferedReader).useDelimiter(newLine);
        } catch (FileNotFoundException | UnsupportedEncodingException | NullPointerException e) {
            e.printStackTrace();
        }

        return scanner;
    }
}