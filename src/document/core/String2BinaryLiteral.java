package document.core;

import java.util.Map;

/**
 * Created by dmitro on 18.03.2015.
 */
public class String2BinaryLiteral {
    public static int transform(String str) {
        byte[] bArr = new byte[SignatureHashSingleton.INSTANCE.getVectorSize()];
        for (int i = 0; i < str.length(); i++) {
            Map<Character, Byte> map = SignatureHashSingleton.INSTANCE.getMap();
            char character = str.charAt(i);
            Byte position = map.get(character);
            if(position != null) {//check if it was valid character
                bArr[position] = (byte) 1;
            }
        }

        StringBuilder strBuilder = new StringBuilder(SignatureHashSingleton.INSTANCE.getVectorSize());
        for (int i = 0; i < SignatureHashSingleton.INSTANCE.getVectorSize(); i++) {
            strBuilder.append(bArr[i]);
        }

        return Integer.parseInt(strBuilder.toString(), 2);
    }

    public static int compare(int transform1, int transform2) {
        int xor = transform1 ^ transform2;
        int count = 0;
        for(int i = 0; i < SignatureHashSingleton.INSTANCE.getVectorSize(); i++) {
            if((xor & 1) == 1) {
                count++;
            }
            xor = xor >> 1;
        }

        return count;
    }
}
