package document.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dmitro on 17.03.2015.
 */
public enum SignatureHashSingleton {
    INSTANCE;

    private final int ENCODED_VECTOR_SIZE = 31;
    private Map<Character, Byte> map;

     SignatureHashSingleton() {
        map = evaluateMap();
     }

    public Map<Character, Byte> getMap() {
        return map;
    }

    public int getVectorSize() {
        return ENCODED_VECTOR_SIZE;
    }

    private Map<Character, Byte> evaluateMap() {
        Map<Character, Byte> charByteMap = new HashMap<>();

        charByteMap.put('а', (byte) 0);
        charByteMap.put('б', (byte) 1);

        charByteMap.put('в', (byte) 2);
        charByteMap.put('г', (byte) 3);
        charByteMap.put('д', (byte) 4);

        charByteMap.put('е', (byte) 5);
        charByteMap.put('ж', (byte) 6);

        charByteMap.put('з', (byte) 7);
        charByteMap.put('і', (byte) 8);
        charByteMap.put('й', (byte) 9);

        charByteMap.put('к', (byte) 10);
        charByteMap.put('л', (byte) 11);

        charByteMap.put('м', (byte) 12);
        charByteMap.put('н', (byte) 13);
        charByteMap.put('о', (byte) 14);

        charByteMap.put('п', (byte) 15);
        charByteMap.put('р', (byte) 16);

        charByteMap.put('с', (byte) 17);
        charByteMap.put('т', (byte) 18);
        charByteMap.put('у', (byte) 19);

        charByteMap.put('х', (byte) 20);

        charByteMap.put('ц', (byte) 21);
        charByteMap.put('ч', (byte) 22);
        charByteMap.put('ш', (byte) 23);

        charByteMap.put('щ', (byte) 24);
        charByteMap.put('ф', (byte) 24);

        charByteMap.put('ь', (byte) 25);

        charByteMap.put('ї', (byte) 26);
        charByteMap.put('є', (byte) 27);
        charByteMap.put('и', (byte) 28);

        charByteMap.put('ю', (byte) 29);
        charByteMap.put('я', (byte) 30);

        return Collections.unmodifiableMap(charByteMap);
    }
}
