package document.core;

import java.util.*;

/**
 * Created by dmitro on 07.03.2015.
 */
public class File2WordsCollection {
    private StemInterface stem;
    private int maxWordsFromFile;

    public File2WordsCollection(StemInterface stem, int maxWordsFromFile) {
        this.stem = stem;
        this.maxWordsFromFile = maxWordsFromFile;
    }

    public List<Word> parseFile() {
        Set<Word> step1 = file2Collection(stem.getScanner());
        List<Word> step2 = sortByCount(step1);
        List<Word> step3 = addHashs(step2);
        List<Word> step4 = dictionaryFilter(step3);
        return step4;
    }

    private List<Word> dictionaryFilter(List<Word> itemList) {
        if (itemList.size() <= maxWordsFromFile) {
            return itemList;
        }

        return itemList.subList(0, maxWordsFromFile);
    }

    private List<Word> addHashs(List<Word> wordList) {
        List<Word> result = new ArrayList<>(wordList.size());

        for (int i = 0; i < wordList.size(); i++) {
            Word word = wordList.get(i);

            if(word.getSpeechPart().isEmpty()) {
                word.setBinaryLiteral(String2BinaryLiteral.transform(word.getValue()));// value (normal string), count and hash will be in DB
            }

            result.add(word);
        }

        return result;
    }

    public void closeAll() {
        stem.closeAll();
    }

    private Set<Word> file2Collection(Scanner scanner) {
        Set<Word> set = new HashSet<>();
        Map<String, Integer> map = new HashMap<>();

        while (scanner.hasNext()) {
            String word = scanner.next();

//            String currToken = scanner.next();
//            String word = stem.extractWordFromToken(currToken);

            if (word.isEmpty()) {
                continue;
            }

            String[] attributes =  word.split("_");

            Word item = new Word();

            item.setValue(attributes[0]);
            if(attributes.length == 2) {
                item.setSpeechPart(attributes[1]);
            }

            item.setCount(1);//TODO: need to reorganize this correct but a little strange logic
            item.setWeight(1.0);

            if (!set.add(item)) {
                set.remove(item);

                Integer newCount = map.get(item.getValue()) + 1;
                map.put(item.getValue(), newCount);
                item.setCount(newCount);

                set.add(item);
            } else {
                map.put(item.getValue(), 1);
            }
        }

        return set;
    }

    private List<Word> sortByCount(Set<Word> words) {
        Comparator<Word> comparator = new Comparator<Word>() {

            @Override
            public int compare(Word o1, Word o2) {
                if (o1.getCount() > o2.getCount()) {
                    return -1;
                }

                if (o1.getCount() < o2.getCount()) {
                    return 1;
                }

                return 0;
            }
        };

        List<Word> list = new ArrayList<Word>(words);
        Collections.sort(list, comparator);
//        SortedSet<DictionaryItem> dictionaryItemTreeSet = new TreeSet<DictionaryItem>(comparator);
//        dictionaryItemTreeSet.addAll(dictionaryItems);

        return list;
    }
}
