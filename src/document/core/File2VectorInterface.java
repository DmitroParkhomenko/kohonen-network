package document.core;

import kohonen.LearningDataModelInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitro on 19.05.2014.
 */

public interface File2VectorInterface extends LearningDataModelInterface {
    void run();
    List<Double[]> getDataList();
}
