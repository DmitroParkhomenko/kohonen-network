package document.core;

import java.util.Scanner;

/**
 * Created by dmitro on 09.03.2015.
 */
public interface StemInterface {
    Scanner getScanner();
    String extractWordFromToken(String currToken);
    void closeAll();
    String getFileName();
}
