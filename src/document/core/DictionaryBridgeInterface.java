package document.core;

import java.io.IOException;
import java.util.List;

/**
 * Created by dmitro on 18.03.2015.
 */

//CRUD — (create read update delete) general interface
// Main purpose is to integrate late DB
public interface DictionaryBridgeInterface {
    void create();

    void read();

    boolean hasNext();

    Word next();

    void push(List<Word> itemList) throws IOException;

    void clear();

    void closeAll();
}
