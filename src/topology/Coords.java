package topology;

/**
 * Class used to get or set neuron coordinate in topology.
 */

public class Coords {
    
    /**
     * x coordinate
     */
    public int x;
    /**
     * y coordinate
     */
    public int y;
    
    /**
     * Creates a new instance of Coords with default coordinates (0,0).
     */
    public Coords() {
        this(0,0);
    }
    
    /**
     * Creates a new instance of Coords with specified <I>x</I> and <I>y</I> coordinate.
     * @param x value of x
     * @param y value of y
     */
    public Coords(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    /**
     * Return <I>x </I>coordinate
     * @return coordinate x
     */
    public int getX(){
        return x;
    }
    
    /**
     * Return<I> y</I> coordinate
     * @return coordinate y
     */
    public int getY(){
        return y;
    }
    
    /**
     * Indicates whether some other object is "equal to" Coords class..
     * @param obj obj - the reference object with which to compare.
     * @return true if this object is the same as the obj argument; false otherwise.
     */
    public boolean equals(Object obj){
        if(obj instanceof Coords){
            Coords coords = (Coords) obj; 
            return (x == coords.x) && (y == coords.y);
        }
        return false;
    }
    
    /**
     * Returns a string representation of the Coords object 
     * <I> [ x = ,y = ]</I>
     * @return Returns a string representation of the Coords object
     */
    public String toString(){
        return "[ x= " + x + ",y= " + y + " ]";
    }
}


