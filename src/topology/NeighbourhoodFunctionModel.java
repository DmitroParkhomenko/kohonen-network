package topology;

/**
 * Neigbbourhood function interface
 */
public interface NeighbourhoodFunctionModel {
    
    /**
     * Return array containing parameters list
     * @return Array containing parameters list
     */
     public Double[] getParameters();
     
    /**
     * Set parameters
     * @param parameters Array contating parameters
     */
     public void setParameters(Double[] parameters);
     
    /**
     * Return valu for specified distance
     * @param distance Distance
     * @return value for specified distance
     */
     public Double getValue(Double distance);
}
