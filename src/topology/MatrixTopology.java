package topology;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Matric Topology is a topology where neurons is set in rows and columns.
 */
public class MatrixTopology implements TopologyModel {
    protected int colNumber, rowNumber;
    private int radius = 0;

    /**
     * Creates a new instance of matrixTopology with specified numbers of rows and columns. 
     * Radius is default set to <I>0</I>
     * @param row number of rows
     * @param col number of columns
     */
    public MatrixTopology(int row, int col) {
        this.rowNumber = row;
        this.colNumber = col;
    }

    /**
     * Creates a new instance of matrixTopology with specified numbers of rows, columns and radius.
     * @param row number of rows
     * @param col number of columns
     * @param radius radius
     */
    public MatrixTopology(int row, int col, int radius) {
        this(row, col);
        this.radius = radius;
    }

   /**
     * Return number of columns
     * @return number of columns
     */
    public int getColNumber() {
        return this.colNumber;
    }

    /**
     * Return Coord object contain intormation about neuron co-ordinate
     * @param neuronNumber neuron number
     * @return coords object
     */
    public Coords getNeuronCoordinate(int neuronNumber) {
        int x = ((neuronNumber - 1) / colNumber) + 1;
        int y = neuronNumber - ((x - 1) * colNumber);

        return new Coords(x, y);
    }

   /**
     * Return neuron number with specyfied co-ordiante
     * @param coords neuron coordinate
     * @return neuron number
     */
    public int getNeuronNumber(Coords coords) {
        if ((coords.x < rowNumber) && (coords.y < colNumber)) {
            return (coords.x - 1) * colNumber + coords.y;
        }

        return -1;
    }

    /**
     * Return number of neuron.
     * @return number of neurons
     */
    public int getNumbersOfNeurons() {
        return colNumber * rowNumber;
    }

     /**
     * Return radius for calculate neighbourhood
     * @return radius
     */
    public int getRadius() {
        return radius;
    }

    /**
     * Return number of rows
     * @return numbers of rows
     */
    public int getRowNumber() {
        return this.rowNumber;
    }

    /**
     * Set number of columns
     * @param colNumber numbers of columns
     */
    public void setColNumber(int colNumber) {
        this.colNumber = colNumber;
    }

    /**
     * Set radius
     * @param radius Radius
     */
    public void setRadius(int radius) {
        this.radius = radius;
    }

    /**
     * Set numbers of rows
     * @param rowNumber numbers of rows
     */
    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    public TreeSet<Integer> neighbourhoodBuilder(int neuronEntryPoint) {
        TreeSet<Integer> result = new TreeSet<Integer>();
        result.add(neuronEntryPoint);
        result.add(-1);
        getNeighbourhoodRecursively(neuronEntryPoint, radius, result);
        result.remove(-1);
        return result;
    }

    private void getNeighbourhoodRecursively(int neuron, int radius, TreeSet<Integer> result) {

        if(radius < 1 ) {
            return;
        }

        int up = getUp(neuron);
        result.add(up);
        if(up != -1) {
            getNeighbourhoodRecursively(up, radius - 1, result);
        }

        int left = getLeft(neuron);
        result.add(left);
        if(left != -1) {
            getNeighbourhoodRecursively(left, radius - 1, result);
        }

        int right = getRight(neuron);
        result.add(right);
        if(right != -1) {
            getNeighbourhoodRecursively(right, radius - 1, result);
        }

        int down = getDown(neuron);
        result.add(down);
        if(down != -1) {
            getNeighbourhoodRecursively(down, radius - 1, result);
        }
    }

    private int getUp(int neuronNumber) {

        if (neuronNumber - colNumber >= 0) {
            return (neuronNumber - colNumber);
        }

        return -1;
    }

    private int getDown(int neuronNumber) {

        if (neuronNumber + colNumber < colNumber * rowNumber) {
            return (neuronNumber + colNumber);
        }

        return -1;
    }

    private int getLeft(int neuronNumber) {

        if ((neuronNumber - 1 >= 0) && ((neuronNumber % colNumber) != 0)) {
            return (neuronNumber - 1);
        }

        return -1;
    }

    private int getRight(int neuronNumber) {

        if ((neuronNumber + 1 < colNumber * rowNumber) && ((neuronNumber + 1) % colNumber != 0)) {
            return (neuronNumber + 1);
        }

        return -1;
    }
}