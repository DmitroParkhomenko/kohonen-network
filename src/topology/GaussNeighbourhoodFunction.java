package topology;


import static java.lang.Math.*;
/**
 * Gauss neighbourhood Function
 */
public class GaussNeighbourhoodFunction implements NeighbourhoodFunctionModel{
    
    private Double r;   // radius
    
    /**
     * Creates a new instance of GaussNeighbourhoodFunction with
     * specified radius
     * @param radius Radius
     */
    public GaussNeighbourhoodFunction(double radius) {
        this.r = radius;
    }
    
    /**
     * Return array containing parameter <I>r</I> - radius
     * @return constant parameter
     */
    public Double[] getParameters(){
        Double[] paremateres = new Double[1];
        paremateres[0] = r;
        return paremateres;    
    }
    
    /**
     * Set parameters. <I>r</I> - radius
     * @param parameters constat value
     */
    public void setParameters(Double[] parameters){
        r = parameters[0];
    }
    
   /**
     * Return value calculated by function describe as 
     * <I>exp(-(k^2))/(2*r^2))</I>
     * @return value of function factor
     * @param distance distance value
     */
    public Double getValue(Double distance){
       return exp(
               -( pow( distance, 2 ) ) / ( 2 * r * r )
       );
    }   
}
