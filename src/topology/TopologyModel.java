package topology;

import java.util.TreeMap;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Topology model interface
 */

public interface TopologyModel {

    /**
     * Return number of columns
     * @return number of columns
     */
    int getColNumber();

    /**
     * Return Coord object containing intormation about neuron co-ordinate
     * @param neuronNumber neuron number
     * @return coords object
     */
    Coords getNeuronCoordinate(int neuronNumber);
    
    /**
     * Return number of neuron.
     * @return number of neurons
     */
    int getNumbersOfNeurons();
    
    /**
     * Return neuron number of specyfied co-ordiante
     * @param coords neuron coordinate
     * @return neuron number
     */
    int getNeuronNumber(Coords coords);
    
    /**
     * Return radius for calculate neighbourhood
     * @return radius
     */
    int getRadius();
    
    /**
     * Return number of rows
     * @return numbers of rows
     */
    int getRowNumber();
    
    /**
     * Set number of columns
     * @param colNumber numbers of columns
     */
    void setColNumber(int colNumber);
    
    /**
     * Set radius
     * @param radius Radius
     */
    void setRadius(int radius);
    
    /**
     * Set number of rows
     * @param rowNumber numbers of rows
     */
    void setRowNumber(int rowNumber);

    TreeSet<Integer> neighbourhoodBuilder(int neuronEntryPoint);
}
