package utils;

import main.Consts;

import java.io.*;
import java.util.*;

/**
 * Created by dmitro on 20.02.2015.
 */
public class NormalizeDataCollection {

    public Double findMaxValue(String path, int[] index2Exlude) {
        Double max = 0.0;

        try (BufferedReader reader = buildReader(path);) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] nums = line.split(",");
                nums = reduce(index2Exlude, nums);

                for(String item: nums) {
                    Double currItem = Double.valueOf(item);
                    if(max < currItem) {
                        max = currItem;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return max;
    }

    private String[] reduce(int[] index2Exlude, String[] nums) {
        List<String> result = new ArrayList<>();
        Set<Integer> set = new HashSet<>();

        for(int item: index2Exlude) {
            set.add(item);
        }

        for(int i = 0; i < nums.length; i++) {
            if(set.contains(i)) {
                continue;
            }

            result.add(nums[i]);
        }

        return result.toArray(new String[result.size()]);
    }


    public void normalizeData(String sourcePath, String targetPath, int[] index2Exlude) {
        Double max = findMaxValue(sourcePath, index2Exlude);

        try (
                BufferedReader reader = buildReader(sourcePath);
                BufferedWriter writer = buildWriter(targetPath);
        ) {
            String oldLine;
            while ((oldLine = reader.readLine()) != null) {
               String[] values = oldLine.split(",");
               values = reduce(index2Exlude, values);
               String newLine = "";

                for(String value: values) {
                    Double oldValue = Double.valueOf(value);
                    Double newValue = oldValue/max;
                    newLine += "," + newValue;
                }

                writer.write(newLine.substring(1));
                writer.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private BufferedWriter buildWriter(String path) throws FileNotFoundException, UnsupportedEncodingException {
        FileOutputStream fileOutputStream = new FileOutputStream(path, false);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, Consts.FILE_ENC);
        return new BufferedWriter(outputStreamWriter);
    }

    private BufferedReader buildReader(String path) throws FileNotFoundException {
        FileInputStream fr = new FileInputStream(path);
        InputStreamReader reader = new InputStreamReader(fr);
        return new BufferedReader(reader);
    }

    public static void main(String[] args) {
        NormalizeDataCollection normalizeDataCollection = new NormalizeDataCollection();
//        int[] index2Exlude = new int[0];
//        System.out.println("Max value = " + normalizeDataCollection.findMaxValue("normalized_3D_spatial_network.txt", index2Exlude));
//        normalizeDataCollection.normalizeData("3D_spatial_network.txt", "normalized_3D_spatial_network.txt");

        int[] index2Exlude = new int[1];
        index2Exlude[0] = 0;
        normalizeDataCollection.normalizeData(
                "kegg_metavolic_relation_network.txt",
                "normalized_kegg_metavolic_relation_network.txt",
                index2Exlude);
    }
}