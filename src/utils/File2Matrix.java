package utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by dmitro on 20.02.2015.
 */
public class File2Matrix {

    public ArrayList<Double[]> convert(String sourcePath, int setSize) {
        ArrayList<Double[]> result = new ArrayList<>();

        try (BufferedReader reader = buildReader(sourcePath)) {
            int currSetSize = 0;

           do {
                String line = reader.readLine();

                if(line == null) {
                    break;
                }

                String[] values = line.split(",");
                Double[] resDoubles = new Double[values.length];
                int index = 0;

                for(String value: values) {
                    resDoubles[index] = Double.valueOf(value);
                    index++;
                }

                result.add(resDoubles);

                currSetSize++;
            } while (currSetSize != setSize);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private BufferedReader buildReader(String path) throws FileNotFoundException {
        FileInputStream fr = new FileInputStream(path);
        InputStreamReader reader = new InputStreamReader(fr);
        return new BufferedReader(reader);
    }

    public static void main(String[] args) {
        File2Matrix file2Matrix = new File2Matrix();
        ArrayList<Double[]> result = file2Matrix.convert("normalized_3D_spatial_network.txt", 100);

        System.out.println("set size = " + result.size());

        for(Double[] item: result) {
            System.out.println(Arrays.toString(item));
        }
    }
}
