package utils;

import neuron.NeuronModel;

import java.util.*;

/**
 * Created by dmitro on 05.04.2015.
 */
public class VectorComparator {
    public static int compare(NeuronModel[] neuronModels) {
        Set<List> set = new HashSet<>();
        for (NeuronModel neuronModel : neuronModels) {
            set.add(Arrays.asList(neuronModel.getWeight()));
        }
        return neuronModels.length - set.size();
    }
}
