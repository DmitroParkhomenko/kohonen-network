package utils;

/**
 * Created by dmitro on 21.02.2015.
 */
public class ConstantFunctorTest {
    public static void main(String[] args) {
        Double y1 = 0.7976874526886921;
        Double x = 0.7978292635984475;
        Double k = 0.8;

        Double y2 = 0.7976874907253861;

        for (int i0 = 0; i0 < 10; i0++) {
            int i1 = 250;
            while (i1 > 0) {
                x = x + k * (y1 - x);
                System.out.println(x);
                i1--;
            }

            int i2 = 250;
            while (i2 > 0) {
                x = x + k * (y2 - x);
                System.out.println(x);
                i2--;
            }
        }
    }
}
