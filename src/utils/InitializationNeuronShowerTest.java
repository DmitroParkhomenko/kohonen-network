package utils;

import efficient_initialization_scheme.InitializationScheme;
import lvq.Cluster;
import metrics.EuclidesMetric;
import neuron.NeuronModel;
import topology.MatrixTopology;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by dmitro on 07.04.2015.
 */
public class InitializationNeuronShowerTest {
    public static void main(String[] args) {
        ArrayList<Cluster> clusters = new ArrayList<>();
        int size = 70;

        for (int j = 0; j < size; j++) {
            Cluster cluster = new Cluster(size, size);

            for (int i = 0; i < size; i++) {
                cluster.setCenter(i, 0.5);
            }

            clusters.add(cluster);
        }

        MatrixTopology topology = new MatrixTopology(size, size, 1);
        InitializationScheme scheme = new InitializationScheme(new EuclidesMetric(), topology);
        NeuronModel[] neuronModels = scheme.run(clusters);

        for (NeuronModel neuronModel : neuronModels) {
            System.out.println(Arrays.toString(neuronModel.getWeight()));
        }

        System.out.println("res = " + VectorComparator.compare(neuronModels));
    }
}
