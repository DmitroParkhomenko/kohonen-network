package neuron;

/**
 * Neuron Model interface
 */
public interface NeuronModel {
    
    /**
     * Returns array contains valu of the weights
     * @return array of the weights
     */
     public Double[] getWeight();

    public int getClusterId();

    public void setClusterId(int clasterId);
    /**
     * Set weigths from array as parameter
     * @param weight array of the weights
     */
     public void setWeight(Double[] weight);
    
    /**
     * Return value of the neuron after activation.
     * if activation function is not set, function 
     * return sum of the multiplication of vector v_i
     * and weight w_i for each i (numbers of weigth and
     * lenght of the input vector)
     * @param inputVector input vector for neuron
     * @return return
     */
    public Double getValue(Double[] inputVector);

}
