package neuron;

import neuron.NeuronModel;

/**
 * Tired Neuon interface. Use to create tiredness neurons
 */
public interface TiredNeuronModel extends NeuronModel {
    /**
     * Return value of tiredness
     * @return value of tiredness
     */
    public int getTiredness();
    
    /**
     * Set tiredness
     * @param tiredness tiredness
     */
    public void setTiredness(int tiredness);
}
