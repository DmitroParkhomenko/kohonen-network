package neuron;

import java.util.Random;

/**
 * Class representing <B>neuron</B> with bias and specyfied 
 * activation function
 */
public class DefaultNeuronWithBias implements NeuronModel {

    private int clusterId = -1;
    /**
     * bias
     */
    private Double bias = 0.0;
    /**
     * array of the weight
     */
    private Double[] weight;

    /**
     * Creates a new instance of DefaultNeuronWithBias with random weights value
     * @param weightNumber numbers of weigths
     * @param maxWeight maximal value of neuron weight
     */
    public DefaultNeuronWithBias(int weightNumber, Double[] maxWeight) {
        if(weightNumber == maxWeight.length){
            Random rand = new Random();
            weight = new Double[weightNumber];
            for(int i=0; i< weightNumber; i++){
                weight[i] = rand.nextDouble() * maxWeight[i];
            }
        }
    }
    
    /**
     * Creates a new instance of DefaultNeuronWithBias with specified weights
     * defined in array
     * @param weightArray array of weights value
     */
    public DefaultNeuronWithBias(Double[] weightArray){
        int weightSize = weightArray.length;
        weight = new Double[weightSize];
        for(int i=0; i< weightSize; i++){
            weight[i] = weightArray[i];
        }
    }

    /**
     * 
     * Returns array contains valu of the weights
     * @return array of the weights
     * 
     */
    public Double[] getWeight() {
        return weight.clone();
    }

    /**
     * 
     * Set weigths from array as parameter
     * @param weight array of the weights
     * 
     */
    public void setWeight(Double[] weight) {
        for (int i=0; i < weight.length; i++ ){
            this.weight[i] = weight[i]; 
        }
    }

    /**
     * 
     * Return value of the neuron after activation.
     * @param inputVector input vector for neuron
     * @return return
     * 
     */
    public Double getValue(Double[] inputVector) {
        Double value = 0.0;
        int inputSize = inputVector.length;
        
        for(int i=0; i< inputSize; i++){
            value = value + inputVector[i] * weight[i];
        }

        return value - bias;
    }

    /**
     * Return value of the bias
     * @return value of the bias
     */
    public Double getBias() {
        return bias;
    }
 
    /**
     * Set bias
     * @param bias value of the bias
     */
    public void setBias(Double bias) {
        this.bias = bias;
   }

    public int getClusterId() {
        return clusterId;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }
}
