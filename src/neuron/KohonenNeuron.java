package neuron;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

import main.Settings;
import metrics.MetricModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class representing <B>neuron</B> with bias and specyfied 
 * activation function
 */

/**
 * Created by dmitro on 04.06.2014.
 */
public class KohonenNeuron implements NeuronModel {

    private static Logger _logger = LoggerFactory.getLogger(KohonenNeuron.class);

    private int clusterId = -1;
    /**
     * array of the weight
     */
    private Double[] weight;
    /**
     * calculates distance beetwen input vector and neurons weigths
     */

    private MetricModel distanceFunction;

    /**
     * Creates new neuron with specyfied numbers of weight.
     * Value of the weight is random where max value is definited by maxWeight.
     * Activation function is definied by activationFunction parameter.
     *
     * @param weightNumber numbers of weight
     * @param maxWeight    max value of the weight
     */
    public KohonenNeuron(int weightNumber, Double[] maxWeight, Settings settings) {

        if(!settings.isRandomWeight() && settings.getInitVector() == null) {
            this.weight = new Double[weightNumber];
            Arrays.fill(weight, 1E-4);
            return;
        }

        if(!settings.isRandomWeight()) {
            this.weight = settings.getInitVector();
            return;
        }

        Random rand = new Random();
        this.weight = new Double[weightNumber];

        for (int i = 0; i < weightNumber; i++) {
            Double nextDouble = rand.nextDouble();
            this.weight[i] = nextDouble * maxWeight[i];
        }
    }

    /**
     * Creates new neuron with weight specyfied by array and specified activation funciton.
     * Numbers of weights are the same as length of the array.
     *
     * @param weightArray array of the weight
     */
    public KohonenNeuron(Double[] weightArray) {
        int weightSize = weightArray.length;
        weight = new Double[weightSize];

        for (int i = 0; i < weightSize; i++) {
            weight[i] = weightArray[i];
        }
    }

    public KohonenNeuron(int weightNumber) {
        weight = new Double[weightNumber];
    }

    public KohonenNeuron() {

    }

    /**
     * Return array of neuron weigths
     *
     * @return array of neuron weigths
     */
    public Double[] getWeight() {
        return weight.clone();
    }

    /**
     * Return value of the neuron after activation.
     * if activation function is not set, function
     * return sum of the multiplication of vector v_i
     * and weight w_i for each i (numbers of weigth and
     * lenght of the input vector)
     *
     * @param inputVector input vector for neuron
     * @return return
     */
    public Double getValue(Double[] inputVector) {
        Double value = 0.0;
        int inputSize = inputVector.length;

        if (distanceFunction != null) {
            value = distanceFunction.getDistance(weight, inputVector);
        } else {
            for (int i = 0; i < inputSize; i++) {
                value = value + inputVector[i] * weight[i];
            }
        }

        return value;
    }

    /**
     * Set weigths from array as parameter
     *
     * @param weight array of the weights
     */
    public void setWeight(Double[] weight) {
//        for (int i=0; i< weight.length; i++){
//            this.weight[i] = weight[i];
//        }
        this.weight = weight.clone();
    }

    /**
     * Returns a string representation of the NeuronModel.
     * NeuronModel is describe by its weights [w_1,w_2,...]
     *
     * @return a string representation of the NeuronModel
     */
    public String toString() {
        String text = "";
        text += "[ ";
        int weightSize = weight.length;
        for (int i = 0; i < weightSize; i++) {
            text += weight[i];
            if (i < weightSize - 1) {
                text += ", ";
            }
        }
        text += " ]";
        return text;
    }

    /**
     * Return reference to distance function
     *
     * @return reference to distance function
     */
    public MetricModel getDistanceFunction() {
        return distanceFunction;
    }

    /**
     * Set distance function
     *
     * @param distanceFunction reference to distance function
     */
    public void setDistanceFunction(MetricModel distanceFunction) {
        this.distanceFunction = distanceFunction;
    }

    public int getClusterId() {
        return clusterId;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }
}
