package neuron;

import main.Settings;

/**
 * Class representing <B> kohenen neuron with tiredness</B> 
 * with specyfied activation function
 */
public class KohonenNeuronWithTired extends KohonenNeuron implements TiredNeuronModel {

    /**
     * Tiredness default set to 10
     */
    int tiredness = 10;
    
    /**
     * Creates new kohonen neuron with specyfied numbers of weight. 
     * Value of the weight is random where max value is definited by maxWeight.
     * Activation function is definied by activationFunction parameter.
     * @param weightNumber numbers of weight
     * @param maxWeight max value of the weight
     */
    public KohonenNeuronWithTired(int weightNumber, Double[] maxWeight, Settings settings) {
        super(weightNumber,maxWeight, settings);
    }
    
    
    /**
     * Creates new kohonen neuron with weight specyfied by array and specified activation funciton. 
     * Numbers of weights are the same as length of the array.
     * @param weightArray array of the weight
     */
    public KohonenNeuronWithTired(Double[] weightArray) {
        super(weightArray);
    }
    
    
    /**
     * Set tiredness
     * @param tiredness tiredness
     */
    public void setTiredness(int tiredness){
        this.tiredness = tiredness;
    }
    
    /**
     * Return value of tiredness
     * @return value of tiredness
     */
    public int getTiredness(){
        return this.tiredness;
    }
}
