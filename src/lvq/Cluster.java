package lvq;

/**
 * Created by dmitro on 31.05.2014.
 */
public class Cluster {
    private Double mCenter[];
    private int mMember[];    // Index of vectors belonging to this cluster.
    private int mNumMembers;

    public Cluster(int vSize, int nPattern) {
        mCenter = new Double[vSize];
        mMember = new int[nPattern];
        mNumMembers = 0;
    }

    public Double getCenter(int index) {
        return mCenter[index];
    }

    public void setCenter(int index, Double cValue) {
        mCenter[index] = cValue;
    }

    public int getMember(int index) {
        return mMember[index];
    }

    public void setMember(int index, int cValue) {
        mMember[index] = cValue;
    }

    public int getNumMembers() {
        return mNumMembers;
    }

    public void setNumMembers(int cValue) {
        mNumMembers = cValue;
    }

    public Double[] getmCenter() {
        return mCenter.clone();
    }
}
