package lvq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import document.core.File2VectorInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmitro on 26.05.2014.
 */
public class LearningVectorQuantization {
    private static Logger _logger = LoggerFactory.getLogger(LearningVectorQuantization.class);
    private final Double THRESHOLD;
    private int maxPatterns;    // Total input patterns.
    private int vectorLength;    // Size of each input pattern.
    private ArrayList<Cluster> cluster;
    private int numClusters; //current clusters number
    private List<Double[]> pattern;

    public LearningVectorQuantization(Double threshold) {
        cluster = new ArrayList<>();
        pattern = new ArrayList<>();
        numClusters = 0;
        THRESHOLD = threshold;
    }

    public ArrayList<Cluster> run(File2VectorInterface docsEncoderUtils) {
        pattern = docsEncoderUtils.getDataList();
        vectorLength = pattern.get(0).length;
        maxPatterns = pattern.size();

        initialize();
        runAlgorithm();
//        showClusters();
       return cluster;
    }

    private void showClusters() {
        _logger.info("\nCluster Centers:");

        for (int i = 0; i < numClusters; i++) {
            String weightText = "[";

            for (int j = 0; j < vectorLength; j++) {
                weightText += cluster.get(i).getCenter(j);

                if (j < vectorLength - 1) {
                    weightText += ", ";
                }
            }

            weightText += "]";
            _logger.info("LVQ: " + weightText);
        }
    }

    private void initialize() {
        cluster = new ArrayList<>();
    }

    private void runAlgorithm() {
        int winner = 0;
        Double dist = 0.0;

        for (int pat = 0; pat < maxPatterns; pat++) {
            // Attach the current input to closest cluster.
            winner = findClosestCluster(pat);

            if (winner == -1) {
                winner = allocateCluster();
            } else {
                dist = eucNorm(pat, winner);
                dist = Math.sqrt(dist);                // get root of the distance measure.

                if (dist > THRESHOLD) {
                    winner = allocateCluster();        // Allocate new cluster.
                    //_logger.info("\n>>> Creating NEW cluster number: " + winner);
                }
            }

            //_logger.info("\nPattern " + pat + " assigned to cluster " + winner);
            attach(winner, pat);                       // Attach pattern to winner.
            calcNewClustCenter(winner);                // Adapt cluster center.
        }
    }

    private int allocateCluster() {
        int n = numClusters;
        numClusters += 1;
        return n;
    }

    private Double eucNorm(int patternIndx, int clusterIndx) {
        // Calculate Euclidean norm of vector, patternIndx.
        Cluster aCluster = null;
        Double dist = 0.0;  // dist is the difference between pattern vector, patternIndx, and cluster center, clusterIndx.

        aCluster = cluster.get(clusterIndx);
        for (int i = 0; i < vectorLength; i++) {
            //Square the difference between patternIndx and clusterIndx
            dist += Math.pow(aCluster.getCenter(i) - getPatternElement(patternIndx, i), 2);
        }

        return dist;
    }

    private int findClosestCluster(int pat) {
        Double minDist = Math.pow(10, 10);   //Just some large number.
        int clustID = -1;

        for (int i = 0; i < numClusters; i++) {
            Double d = eucNorm(pat, i);

            if (d < minDist) {
                minDist = d;
                clustID = i;
            }
        }

        return clustID;
    }

    private void attach(int c, int p) {

        if(c >= cluster.size()) {
            Cluster aCluster = new Cluster(vectorLength, maxPatterns);
            cluster.add(aCluster);
        }

        Cluster aCluster = cluster.get(c);

        int memberIndex = aCluster.getNumMembers();
        aCluster.setMember(memberIndex, p);
        aCluster.setNumMembers(aCluster.getNumMembers() + 1);
    }

    private void calcNewClustCenter(int c) {
        int vecID = 0;
        int nMembers = 0;
        Cluster aCluster = cluster.get(c);
        Double tmp[] = new Double[vectorLength];

        for (int j = 0; j < vectorLength; j++) {
            tmp[j] = 0.0;
        }

        nMembers = aCluster.getNumMembers();
        for (int j = 0; j < nMembers; j++)                  // cycle through member vectors
        {
            vecID = aCluster.getMember(j);
            for (int k = 0; k < vectorLength; k++)         // cycle through elements of vector
            {
                //_logger.info("Cluster " + c + " Pattern[" + vecID + "][" + k + "] = " + getPatternElement(vecID, k) + ", Member_ID = " + vecID);
                tmp[k] += getPatternElement(vecID, k);               // add (member) pattern element into temp
            } // k
        } // j

        for (int k = 0; k < vectorLength; k++)             //cycle through elements of vector
        {
            tmp[k] = tmp[k] / aCluster.getNumMembers();
            aCluster.setCenter(k, tmp[k]);
        } // k
    }

    private Double getPatternElement(int i, int j) {
        return pattern.get(i)[j];
    }

    private void setPatternElement(int i, int j, Double v) {
        pattern.get(i)[j] = v;
    }
}
